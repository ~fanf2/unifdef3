/*             _  __    _      __
 *   _  _ _ _ (_)/ _|__| |___ / _|  unifdef
 *  | || | ' \| |  _/ _` / -_)  _|  expand macros
 *   \_,_|_||_|_|_| \__,_\___|_|    reduce #ifdefs
 *
 * Written by Tony Finch <dot@dotat.at>
 * SPDX-License-Identifier: 0BSD OR MIT-0
 */

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <inttypes.h>
#include <limits.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

/*
 * re2c doesn't generate a #line directive after a !include,
 * so we follow !include with !ignore to get correct line
 * numbers in compiler errors and the TAGS file
 */

/*!include:re2c "pragma.h" *//*!ignore:re2c
  #line please */

/*

Standards
---------

Below we refer to either the working group (WG14) if the reference is
the same for all the documents, or the document number if it varies.
Details are noted as "implementation-defined" where relevant to
either C or C++.

ISO/IEC JTC1/SC22/WG14	- http://www.open-std.org/jtc1/sc22/wg14/

WG14/N1256 (C99:TC3)
WG14/N1570 (C11)
WG14/N2176 (C18)
WG14/N3220 (C23)

C11 and C18 only differ in details; C23 has more changes, in
particular the preprocessor has some new features.

The numbering of footnotes is not stable so we refer to them with
a * after the paragraph number containing the footnote anchor.

ISO/IEC JTC1/SC22/WG21	- http://www.open-std.org/jtc1/sc22/wg21/

WG21/N3337 (C++11)
WG21/N4296 (C++14)
WG21/N4659 (C++17)
WG21/N4860 (C++20)

Section numbers in the C++ standards are not stable (for example,
lexical conventions moved from §2 to §5) so we use named [anchors]
in references.


General principles
------------------

Buffers usually live through two phases: write-only (during which they
can move), and read-only (during which they are fixed). They contain a
sentinel '\0' after the used portion to protect against overscan.

A buffer can live through more phases due to rescanning the result of
macro expansion, when the result contains an incomplete function-like
macro invocation. To ensure all the arguments are contiguous in
memory, the expander needs to append the rest of the invocation from
the following code to its buffer. The append can cause reallocation,
and hide sets need to survive, so they have to be based on buffer
offsets not absolute pointers.

A symbol can be a macro, a defined values, or explicitly undefined.
Whether a word is a known or unknown symbol is determined by the symbol
table constructed from the command line options; whether it is an
identifier or a keyword is fixed by the lexer, and a symbol can be
either.

There are a few sections where code is automatically maintained by a
script, to ensure they are kept in sync with the rest of the code
without too much manual effort.

*/

/***********************************************************************
 *      _        _               _   _
 *   __| |___ __| |__ _ _ _ __ _| |_(_)___ _ _  ___
 *  / _` / -_) _| / _` | '_/ _` |  _| / _ \ ' \(_-<  types and
 *  \__,_\___\__|_\__,_|_| \__,_|\__|_\___/_||_/__/  functions
 */

/*
 * Characters are better unsigned. The u8 prefix looks at first like it
 * might be helpful, but turns out not to be. For character constants its
 * type is unsigned char and its value is limited to a code points that fit
 * in a single code unit, which is OK for unsigned ASCII. But for strings
 * its type is plain char, and in fact the C11 standard says a UTF–8 string
 * literal is the same as a normal string except for the prefix. (In C++
 * and C23, u8"strings" are unsigned.) So, sadly we will just have to
 * scatter casts around like Philip Hazel did in Exim.
 */
typedef       unsigned char byte;
typedef                char *SS;	/* cast to signed string */
typedef const          char *SC;	/* cast to string constant */
typedef       unsigned char *BS;	/* cast to byte string */
typedef const unsigned char *BC;	/* cast to byte constant */

/*
 * Forward declarations of various types.
 *
 * This includes the automatically-generated Token enum.
 */
/*!include:re2c "forward.h" *//*!ignore:re2c
  #line please */

/*
 * Values for the exit status are documented in the manual, so they are
 * written here explicitly. BigProblem occurs when we combine exit
 * status values using `or`; it's semantically the same as Problem and
 * gets normalized to Problem before we exit, but it has a bigger value.
 *
 * A status of OK is used when reading the -f header file, since at that
 * point we aren't trying to change anything.
 */
typedef enum Status {
	OK = 0,
	Unchanged = 0,
	Modified = 1,
	Problem = 2,
	BigProblem = 3,
} Status;

/*
 * Types of symbols (and header names), corresponding to the set of
 * preprocessor directives that can appear in unifdef -f header files.
 */
typedef enum Symtype {
	Undefined,
	NoDefines,
	NoIncludes,
	ValueOnly,
	ObjectLike,
	FunctionLike,
	VariadicFun,
} Symtype;

/*
 * The controlling expression of a #if is either intmax_t or uintmax_t,
 * Signed or Unsigned. If there is an unknown symbol its value is Unknown.
 * If there is a syntax error we propagate it upwards/outwards so we give
 * up on the whole expression. There can also be evaluation errors, which
 * are a kind of side-effect (division by zero is the most obvious case)
 * so they affect shortcutting evaluation of && || ?:
 *
 * The simplifier applies to boolean && || ! ?: subexpressions. We
 * would like to delete them entirely, but when a simplified operator
 * is a subexpression of a non-boolean operator, we need to substitute
 * its value instead. So we simplify somewhat lazily by keeping track
 * of boolean values as a separate subtype, and simplifying them
 * depending on how their value is demanded.
 *
 * The result type of boolean operators is int in C and bool in C++,
 * either of which are equivalent to intmax_t in the preprocessor, so
 * we don't need to track boolean signedness. Note that we mostly
 * don't use our Boolean type for the result of comparison operators,
 * because they aren't simplified.
 */
typedef enum Inttype {
	SyntaxError,
	ValueError,
	Unknown,
	Boolean,
	Signed,
	Unsigned,
} Inttype;

/*
 * How part of an expression is removed when it is being simplified.
 * We re-use the term "promotion" for the situation of a lazy boolean
 * simplification whose value has been demanded (the "nothing but" cases).
 * The "nothing at all" deletions happen when we can eagerly simplify.
 */
typedef enum Substitute {
	NothingBut0,
	NothingBut1,
	NothingAtAll,
} Substitute;

/*
 * The result of evaluating a #if expression.
 */
typedef enum Tristate {
	Maybe = -1,
	False =  0,
	True  = +1,
} Tristate;

/*
 * Possible states when working our way through a preprocessor if-section,
 * explained in more detail in the "preprocess" section below. These values
 * are used to index `Directif` arrays of state transition functions.
 */
typedef enum IfState {
	KeepOutside,
	KeepTrueHead,	// first non-false #(el)if is true
	KeepTrueBody,	// a true #elif after a maybe state
	KeepMaybeBody,	// first non-false #(el)if is maybe
	KeepElseHead,	// #else after only false #(el)ifs
	KeepElseBody,	// #else after a false or maybe state
	DropFalseHead,	// false #if followed by false #elifs
	DropFalseBody,	// a false #elif after a maybe state
	DropElifTail,	// #elif to delete but keep #endif
	DropElseTail,	// #else to delete but keep #endif
	DropElifNoEnd,	// #elif that disappears completely
	DropElseNoEnd,	// #else that disappears completely
	IfArraySize,
	KeepMin = KeepOutside,
	KeepMax = KeepElseBody,
	DropMin = DropFalseHead,
	DropMax = DropElseNoEnd,
} IfState;

/*
 * The if-section state transition functions and tables.
 */

typedef IfState hash_fn(Output *out, Position *here, IfState state);

typedef hash_fn *Directif[IfArraySize];

/*
 * Prototypes of token dispatch table functions. There's a longer
 * discussion of these functions in the comment above the dispatch table.
 *
 * null denotation - evaluate a token ignoring what is to the left
 * left denotation - evaluate a token with some left context
 * binary ops, signed and unsigned
 * show pretty prints tokens
 * typing and undefined behaviour logic
 *
 * tub functions often have silly names inspired by the undefined
 * behaviour that they deal with, so they don't parallel the others.
 */

#define NUD(NAME) Integer nud_##NAME(Expression *e)
#define LED(NAME) Integer led_##NAME(Expression *e, Integer left)
#define BINS(NAME) intmax_t bins_##NAME(intmax_t left, intmax_t right)
#define BINU(NAME) uintmax_t binu_##NAME(uintmax_t left, uintmax_t right)
#define SHOW(NAME) void show_##NAME(span s)

typedef NUD(fn);
typedef LED(fn);
typedef BINS(fn);
typedef BINU(fn);
typedef SHOW(fn);

typedef Inttype tub_fn(Integer left, Integer right);

// alias for use in the dispatch table
#define U_A_C usual_arithmetic_conversions

/*
 * Automatically generated forward declarations of all functions.
 */
/*!include:re2c "functions.h" *//*!ignore:re2c
  #line please */

/*
 * Ensure that NULL == xalloc(p, 0) and NULL != xalloc(p, !0)
 */
static void *
xalloc(void *ptr, size_t size) {
	if(size == 0) {
		free(ptr);
		return(NULL);
	}
	ptr = realloc(ptr, size);
	if(ptr == NULL)
		fale("malloc");
	return(ptr);
}

#define FREE(ptr) ((ptr) = xalloc((ptr), 0))

/***********************************************************************
 *      _ _                       _   _
 *   __| (_)__ _ __ _ _ _  ___ __| |_(_)__ ___
 *  / _` | / _` / _` | ' \/ _ (_-<  _| / _(_-<  standard
 *  \__,_|_\__,_\__, |_||_\___/__/\__|_\__/__/  error
 *              |___/
 */

#define NEVER(x) assert((x) == 0)

#define eprintf(...) fprintf(stderr, __VA_ARGS__)

static const char *progname;

/*
 * from the -d command line option
 */
static Set *dflags;

/*
 * If a flag is lower-case, it is on by default, and vice versa for
 * upper-case, so and -dx makes unifdef quieter -dX makes it louder.
 */
static bool
dflag(char flag) {
	return(set_has(dflags, (Index)flag) == !islower(flag));
}

/*
 * This is for messages that aren't associated with an input file;
 * for those, see diagnostic() in the parser section below.
 */
static bool
dmesg(const char *severity, char flag) {
	if(!dflag(flag)) return(false);
	eprintf("%s: %s(%c): ", progname, severity, flag);
	return(true);
}

#define WAIL do {					\
		va_list ap;				\
		va_start(ap, fmt);			\
		eprintf("%s: ", progname);		\
		vfprintf(stderr, fmt, ap);		\
	} while(0)

static void noreturn like_printf(1,2)
fail(const char *fmt, ...) {
	WAIL;
	putc('\n', stderr);
	exit(2);
}

static void noreturn like_printf(1,2)
fale(const char *fmt, ...) {
	WAIL;
	eprintf(": %s\n", strerror(errno));
	exit(2);
}

/***********************************************************************
 *    __      _               _     _
 *   / _|__ _| |_   _ __  ___(_)_ _| |_ ___ _ _ ___
 *  |  _/ _` |  _| | '_ \/ _ \ | ' \  _/ -_) '_(_-<  strings that know
 *  |_| \__,_|\__| | .__/\___/_|_||_\__\___|_| /__/  how big they are
 *                 |_|
 */

/*
 * It seems to be less error-prone to work with start and end pointers
 * than with a mixture of pointers and lengths.
 */
typedef struct span {
	const byte *ptr, *end;
} span;

static bool
span_is_null(span s) {
	return(s.ptr == NULL || s.end == NULL);
}

static size_t
span_len(span s) {
	return((size_t)(s.end - s.ptr));
}

/*
 * Constructors
 */

static span
span_str(const char *str) {
	return((span){ (BC)str, (BC)strchr(str, '\0') });
}

static span
span_size(const void *ptr, size_t size) {
	return((span){ (BC)ptr, (BC)ptr + size });
}

#define assert_span2(left, right)				\
	assert(!span_is_null(left) && !span_is_null(right))

/*
 * Embiggen a span
 */
static span
span2(span left, span right) {
	assert_span2(left, right);
	assert(left.ptr <= right.end);
	return((span){ left.ptr, right.end });
}

/*
 * The left span plus everything up to but not including the right span.
 */
static span
span_left(span left, span right) {
	assert_span2(left, right);
	assert(left.ptr <= right.ptr);
	return((span){ left.ptr, right.ptr });
}

/*
 * Everything after the left span up to and including the right span.
 */
static span
span_right(span left, span right) {
	assert_span2(left, right);
	assert(left.end <= right.end);
	return((span){ left.end, right.end });
}

/*
 * Trim the outer span by removing the left or right part covered by the
 * inner span.
 */
static span
span_trim(span outer, span inner) {
	if(outer.ptr == inner.ptr) return(span_right(inner, outer));
	if(outer.end == inner.end) return(span_left(outer, inner));
	assert(outer.ptr == inner.ptr || outer.end == inner.end);
	NEVER("span_trim");
}

/*
 * Syntactic shenanigans for %.*s print formats in error or debugging messages
 */
#define spanf(s) (int)span_len(s), (s).ptr

/***********************************************************************
 *    __ _          _ _    _
 *   / _| |_____  _(_) |__| |___   __ _ _ _ _ _ __ _ _  _ ___
 *  |  _| / -_) \/ / | '_ \ / -_) / _` | '_| '_/ _` | || (_-<  growable
 *  |_| |_\___/_/\_\_|_.__/_\___| \__,_|_| |_| \__,_|\_, /__/ structures
 *                                                   /__/
 */

typedef size_t Index;

#define MISSING SIZE_MAX

#define Flexible size_t size, used

/*
 * Indexing helpers for any flexible type. A flexible array can be
 * initialized to NULL and we can treat NULL and empty arrays as
 * equivalent. INSIDE() is nice for loop conditions as well as ad-hoc
 * bounds checking.
 */
#define AVAIL(flex) ((flex) == NULL ? 0 : (flex)->size - (flex)->used)
#define INSIDE(flex,index) ((flex) != NULL && (flex)->used > (index))

// expandable buffers --------------------------------------------------

typedef struct Buffer {
	Flexible;
	byte base[];
} Buffer;

static byte *
bufend(Buffer *buf) {
	return(buf->base + buf->used);
}

static span
span_buf(Buffer *buf) {
	return(span_size(buf->base, buf->used));
}

static Buffer *
buf_more(Buffer *buf, size_t more) {
	/* Buffers are used for collecting the results of macro expansion,
	   so their minimum size does not want to be very large */
	return(flexalloc(buf, sizeof(byte), 16, more));
}

static Buffer *
buf_cat(Buffer *buf, span s) {
	size_t len = span_len(s);
	buf = buf_more(buf, len + 1); /* sentinel */
	memcpy(bufend(buf), s.ptr, len);
	buf->used += len;
	return(buf);
}

static Buffer *
buf_slurp(const char *fn) {
	Buffer *b = NULL;
	FILE *fp;
	if(strcmp(fn, "-") == 0) fp = stdin;
	else fp = fopen(fn, "rb");
	if(fp == NULL)
		fale("open %s", fn);
	while(!feof(fp)) {
		b = buf_more(b, 256); // exercise the allocator a bit
		b->used += fread(bufend(b), 1, AVAIL(b), fp);
		if(ferror(fp))
			fale("read %s", fn);
	}
	fclose(fp);
	b = buf_more(b, 1); /* sentinel */
	return(b);
}

static void
buf_spew(Buffer *buf, const char *fn) {
	FILE *fp;
	if(strcmp(fn, "-") == 0) fp = stdout;
	else fp = fopen(fn, "wb");
	size_t n = fwrite((SC)buf->base, sizeof(byte), buf->used, fp);
	if(n < buf->used || fclose(fp) < 0)
		fale("write %s", fn);
}

// list of buffers -----------------------------------------------------

/*
 * List of buffers to be free()d later, used to keep the backing store
 * for symbol tables. buflist_add() is idempotent so that we don't
 * risk double free()s, though we don't need this just for symbol tables.
 */

typedef struct Buflist {
	Flexible;
	Buffer *buf[];
} Buflist;

static Buflist *
buflist_add(Buflist *list, Buffer *buf) {
	for(Index i = 0; INSIDE(list,i); i++)
		if(list->buf[i] == buf)
			return(list);
	list = flexalloc(list, sizeof(list->buf[0]), 1, 1);
	list->buf[list->used++] = buf;
	return(list);
}

static Buflist *
buflist_free(Buflist *list) {
	for(Index i = 0; INSIDE(list, i); i++)
		FREE(list->buf[i]);
	return(xalloc(list, 0));
}

// unbounded set of bits -----------------------------------------------

typedef struct Set {
	Flexible;
	byte octet[];
} Set;

#define OCTET_BIT Index o = i / 8, b = i % 8

static bool
set_has(Set *s, Index i) {
	OCTET_BIT;
	return(INSIDE(s,o) && (s->octet[o] & 1<<b) != 0);
}

static Set *
set_add(Set *s, Index i) {
	OCTET_BIT;
	s = flexalloc(s, sizeof(byte), o+1, 0);
	s->used = s->size;
	s->octet[o] |= 1<<b;
	return(s);
}

static Set *
set_flags(Set *s, const byte *p) {
	while(*p != '\0')
		s = set_add(s, *p++);
	return(s);
}

#undef OCTET_BIT

// hash table ----------------------------------------------------------

/*
 * A super simple hashed list of spans, for symbols and other kinds of
 * tokens, e.g. header names.
 *
 * We only append entries to the table and search linearly, which is not
 * at all efficient if the table gets big (though we don't expect it
 * to), but it preserves insertion order which is handy when we use it
 * for a list of macro parameters.
 *
 * (So yes, we get quadratic with very large numbers of symbols,
 * but not _accidentally_ quadratic.)
 */

typedef size_t Hash;

typedef struct Table {
	Flexible;
	struct {
		Hash h;
		span s;
	} list[];
} Table;

/*
 * Unlike other flexible arrays, we want to make a distinction between
 * an empty array (no macro parameters) and no array at all (not a
 * function-like macro).
 */
static Table *
tab_new(void) {
	Table *t = NULL;
	t = flexalloc(t, sizeof(t->list[0]), 0, 0);
	return(t);
}

static Table *
tab_hadd(Table *t, Hash h, span s) {
	t = flexalloc(t, sizeof(t->list[0]), 2, 1);
	t->list[t->used].h = h;
	t->list[t->used].s = s;
	t->used++;
	return(t);
}

static Index
tab_hfind(Table *t, Hash h, span s) {
	for(Index i = 0; INSIDE(t,i); i++)
		if(t->list[i].h == h && tokeq(t->list[i].s, s))
			return(i);
	return(MISSING);
}

static Table *
tab_add(Table *t, span s) {
	return(tab_hadd(t, tokhash(s), s));
}

static Index
tab_find(Table *t, span s) {
	return(tab_hfind(t, tokhash(s), s));
}

/*
 * Hash table entries point into buffers, so when we are comparing
 * spans we need to allow for the fact that tokens can be arbitrarily
 * interrupted by backslash-newline.
 */

static Hash
tokhash(span txt) {
	/* DJBx33a */
	Hash h = 5381;
	for(const byte *p = backslash_newline(txt.ptr);
	    p < txt.end; p = backslash_newline(p + 1))
		h = h*33 + *p;
	return(h);
}

static bool
tokeq(span txt1, span txt2) {
	const byte *p1 = backslash_newline(txt1.ptr);
	const byte *p2 = backslash_newline(txt2.ptr);
	for(;;) {
		if(*p1 != *p2) return(false);
		p1 = backslash_newline(p1 + 1);
		p2 = backslash_newline(p2 + 1);
		if(p1 >= txt1.end && p2 >= txt2.end) return(true);
		if(p1 >= txt1.end || p2 >= txt2.end) return(false);
	}
}

// known symbols -------------------------------------------------------

/*
 * A known symbol has a type and a definition or undefinition. Details
 * of the known symbols are held in an array that parallels a hash
 * table. This allows us to reuse the Table type and have different
 * kinds of associated values for macros and for macro arguments, or no
 * values for a list of macro parameters.
 *
 * We don't include the name of the symbol here: that is stored in a
 * hash table alongside (see Symtab below).
 */

typedef struct Symbol {
	Symtype type;
	Table *param;
	span body;
} Symbol;

typedef struct Symbols {
	Flexible;
	Symbol sym[];
} Symbols;

static Symbols *
sym_add(Symbols *t, Symbol sym) {
	/* start off small like a symbol table */
	t = flexalloc(t, sizeof(t->sym[0]), 2, 1);
	t->sym[t->used++] = sym;
	return(t);
}

static bool
is_undef(Symtype type) {
	return(type == Undefined ||
	       type == NoDefines ||
	       type == NoIncludes);
}

// symbol table --------------------------------------------------------

/*
 * Two parallel flexible arrays: a list of symbol spellings in a hash
 * table, and a list of their meanings.
 *
 * We also keep a list of the buffers that we read symbols from, so that
 * we can free the memory later. It isn't _that_ important to avoid this
 * leak when we are running normally, but it's necessary when we are
 * doing in-process fuzzing.
 */

typedef struct Symtab {
	Symbols *s;
	Table *t;
	Buflist *buflist;
} Symtab;

static Symbol *
sym_find(Symtab *st, span name) {
	Index i = tab_find(st->t, name);
	if(i == MISSING) return(NULL);
	assert(INSIDE(st->s, i));
	return(&st->s->sym[i]);
}

static void
symtab_free(Symtab *st) {
	for(Index i = 0; INSIDE(st->s, i); i++)
		FREE(st->s->sym[i].param);
	FREE(st->s);
	FREE(st->t);
	st->buflist = buflist_free(st->buflist);
}

static void
symtab_debug(Symtab *st) {
	if(!dmesg("debug", 'T')) return;
	eprintf("list of known symbols\n");
	for(Index i = 0; INSIDE(st->t, i); i++) {
		assert(INSIDE(st->s, i));
		span name = st->t->list[i].s;
		Symbol *sym = &st->s->sym[i];
		const char *kw;
		switch(sym->type) {
		case(VariadicFun):
		case(FunctionLike):
			eprintf("#defval %.*s(", spanf(name));
			Table *p = sym->param;
			for(Index j = 0; INSIDE(p, j); j++)
				eprintf("%s%.*s", j == 0 ? "" : ", ",
					 spanf(p->list[j].s));
			eprintf("%s) %.*s\n",
				 sym->type == VariadicFun
				 ? "..." : "",
				 spanf(sym->body));
			continue;
		case(ObjectLike):
			eprintf("#define %.*s %.*s\n",
				spanf(name), spanf(sym->body));
			continue;
		case(ValueOnly):
			eprintf("#defval %.*s %.*s\n",
				spanf(name), spanf(sym->body));
			continue;
		case(Undefined):  kw = "undef";      goto kw;
		case(NoDefines):  kw = "nodefines";  goto kw;
		case(NoIncludes): kw = "noincludes"; goto kw;
		kw:	eprintf("#%s %.*s\n", kw, spanf(name));
			continue;
		}
	}
}

// expression simplifications ------------------------------------------

/*
 * When part of an expression can be deleted, a span covering the
 * subexpression is added to a list of simplifications which are
 * performed (if necessary) after evaluation is complete.
 */

typedef struct Deleted {
	Flexible;
	struct {
		span s;
		Substitute nothing;
	} del[];
} Deleted;

static Deleted *
deleted(Deleted *d, span s, Substitute nothing) {
	d = flexalloc(d, sizeof(d->del[0]), 2, 1);
	d->del[d->used].s = s;
	d->del[d->used].nothing = nothing;
	d->used++;
	return(d);
}

// stack of if-section states ------------------------------------------

/*
 * Rather than using the C call stack, keep track of nesting in this
 * (empty ascending) stack.
 */

typedef struct IfStack {
	Flexible;
	IfState state[];
} IfStack;

static IfStack *
push_if(IfStack *stack, IfState state) {
	stack = flexalloc(stack, sizeof(stack->state[0]), 1, 1);
	stack->state[stack->used++] = state;
	return(stack);
}

static IfState
pop_if(IfStack *stack) {
	assert(stack != NULL && stack->used > 0);
	return(stack->state[--stack->used]);
}

// growable flexible arrays --------------------------------------------

/*
 * We use the "special guarantee" about a union containing several
 * structures that share a common initial sequence (WG14 6.5.2.3p6)
 * to implement somewhat generic growable flexible arrays.
 *
 * All the type-punned structures with Flexible prefixes have to
 * appear in this union, which is ensured by the automaint script.
 */

typedef union Flex {
	struct	{ Flexible; };
	// union of flexible arrays maintained automatically
	Buffer	buffer;
	Buflist	buflist;
	Deleted	deleted;
	IfStack	ifstack;
	Set	set;
	Symbols	symbols;
	Table	table;
	// end of maintained section
} Flex;

/*
 * The actual uppermost limit on the size of an object, to avoid
 * undefined behaviour in pointer arithmetic, is determined by
 * (signed) ptrdiff_t not by (unsigned) size_t.
 */
#define FLEX_MAX(elem) ((PTRDIFF_MAX - sizeof(Flex)) / (elem))

/*
 * avoid arithmetic overflow when comparing size < used + more
 */
static bool
overflow(size_t size, size_t need, size_t used, size_t more) {
	return(size < need || size < used || size - used < more);
}

/*
 * safely and exponentially increase an allocation size
 * until it has enough space for (used + more) * elem
 */
static size_t
embiggen(size_t size, size_t need, size_t used, size_t more, size_t elem) {
	while(overflow(size, need, used, more)) {
		size_t grow = size/2 + 1; /* nonzero */
		assert(!overflow(FLEX_MAX(elem), 0, size, grow));
		size += grow;
	}
	return(size);
}

/*
 * Grow a flexible array if necessary: `elem` is the size of elements
 * in the array, `need` is the minimum number of elements, and space
 * for `more` elements is required after the currently used space.
 */
static void *
flexalloc(void *v, size_t elem, size_t need, size_t more) {
	Flex *f = v;
	size_t size = f ? f->size : 0;
	size_t used = f ? f->used : 0;
	if(overflow(size, need, used, more)) {
		size = embiggen(size, need, used, more, elem);
		f = xalloc(f, sizeof(*f) + size * elem);
		if(v == NULL) memset(f, 0, sizeof(*f));
		memset((BS)(f+1) + used * elem, 0, (size - used) * elem);
		f->size = size;
		f->used = used;
	}
	return(f);
}

/***********************************************************************
 *   _          _         _                  _         _
 *  | |_____  _(_)__ __ _| |  __ _ _ _  __ _| |_  _ __(_)___
 *  | / -_) \/ / / _/ _` | | / _` | ' \/ _` | | || (_-< (_-<  lexical
 *  |_\___/_/\_\_\__\__,_|_| \__,_|_||_\__,_|_|\_, /__/_/__/  analysis
 *                                             /__/
 */

/*
 * WG14 6.4		Lexical elements
 * WG14 6.10		Preprocessing directives
 * WG14 A.1		Lexical grammar
 * WG21 [lex.pptoken]	Preprocessing tokens
 * WG21 [cpp.pre]	Preprocessing directives
 * WG21 [gram.lex]	Lexical conventions
 *
 * Our set of token types does not match the standards.
 *
 * WG14 5.1.1.2		Translation phases
 * WG21 [lex.phases]	Phases of translation
 *
 * Differing from phase 3, we need to preserve whitespace and comments, so
 * they are returned by the lexer as distinct tokens. (Though spans of
 * horizontal whitespace are represented by a single token of type SPACE.)
 *
 * WG14 6.4.9		Comments
 * WG21 [lex.comment]	Comments
 *
 * Note that as specified in the standards the newline terminating a //
 * comment is not part of the comment. This is handy when parsing
 * preprocessor lines since we only need to check for newline tokens (we
 * don't need to look for // comments as well).
 *
 * WG14 6.4.4.1		Integer constants
 * WG14 6.10.1		Conditional inclusion
 * WG14 6.10.4		Line control
 * WG21 [lex.icon]	Integer literals
 * WG21 [cpp.cond]	Conditional inclusion
 * WG21 [cpp.line]	Line control
 *
 * For evaluating #if expressions, preprocessing tokens are not specific
 * enough since they do not include integer constants.
 *
 * To help the evaluator we have different types for constants depending on
 * the base, and with or without an unsigned suffix. We can ignore any
 * length suffix since the evaluator only uses maximum-size integers.
 *
 * Numbers containing ' digit separators (which we should be able to
 * evaluate but can't) have distinct tokens so they can be treated as
 * evaluation errors, not syntax errors, and so they can be filtered out by
 * the differential fuzz tester.
 *
 * ALso to help with differential fuzz testing, we distinguish between LL
 * numbers, which are evaluated the same by the preprocessor and the
 * compiler, and others which are not.
 *
 * There are also a couple of special cases for #line directives where
 * numbers must be bare digit strings. See keep_line() for more commentary.
 *
 * WG14 6.4.4.4		Character constants
 * WG21 [lex.ccon]	Character literals
 *
 * Similarly, we have different types for different forms of character
 * constant, which correspond to the different functions that evaluate
 * them.
 *
 * There is quite a lot of faff to avoid the traps and pitfalls caused by
 * character constants that are disallowed by the standards. If we give a
 * definite value to a constant, we may end up deleting code that should
 * instead have been passed through for the compiler to choke on.
 *
 * Each character token types has a fairly precise lexical syntax, which
 * means its evaluator doesn't have to do very much checking. The main
 * thing we fail to check is disallowed Unicode code points.
 *
 * UTF-8 multibyte character character are evaluated, but multicharacter
 * constants are not. (Ironically, u8'*' character literals are required
 * to contain exactly one code unit, which means they are actually ASCII
 * character literals.)
 *
 * WG14 6.4.1		Keywords
 * WG14 7.9		Alternative spellings
 * WG21 [lex.key]	Keywords
 * WG21 [tab:lex.key.digraph] Alternative representations
 *
 * We don't need language keywords (while volatile switch etc.), but it is
 * helpful to recognise preprocessing directive names and non-punctuation
 * operators as different from identifiers. The names we use for operators
 * match iso646.h when relevant, so that it's easier to ensure evaluation
 * is the same for keyword operators and matching punctuation operators.
 *
 * WG14 6.4.6		Punctuators
 * WG14 6.6p3		Constant expressions
 * WG21 [lex.operators]	Operators and punctuators
 * WG21 [lex.digraph]	Alternative tokens
 *
 * We only need to accurately lex operators that can be evaluated by
 * #if or that can cause #if to be misparsed (i.e. ++ and --), which
 * means we can ignore bracket digraphs, assignments, and member
 * operators. C++20 added the <=> operator; its result is not
 * straightforwardly compatible with intmax_t so it also cannot be
 * used in #if expressions.
 *
 * When an operator such as += appears in a #if, we will encounter a
 * parse error when trying to evaluate it, so the result will be
 * erroneous and the #if will be passed through for the compiler
 * to complain about.
 *
 * WG14 6.4p3		categories of preprocessing token
 * WG21 [lex.pptoken]	Preprocessing tokens
 *
 * The Token enum is automatically generated from the token types returned
 * by the lexer, so there are a few extra token names mentioned in dead
 * code, to be collected by the automaint script.
 */
typedef enum Token Token;

/*
 * N2596 6.10.4p7	Line control - recommended practice
 *
 * C20 recoomends that a token's line number should be the line of its
 * first character. This matters more for raw strings than anything else,
 * but C++ doesn't have a similar recommendation.
 *
 * We aren't bothering with character-accurate error messages, but if we
 * wanted to we could make this into a structure containing a pointer to
 * the start of the line as well.
 */
typedef size_t Lino;

/*
 * A position structure keeps track of our lexing / parsing location
 * in a file or macro expansion.
 *
 * `name` - the current filename, typically initialized from a buffer
 *          name and updated by #line directives
 *
 * `src` - the part of a buffer that we are scanning, either the whole
 *         buffer, or for instance a macro body.
 *
 * `tok` - the type of the current lookahead token
 *
 * `txt` - a span covering the spelling of the current lookahead token
 *
 * `lino`, `linext` - line numbers of the current lookahead token
 *
 * We keep line numbers for both ends of the token, i.e the line
 * number of the current lookahead token, and the line number for the
 * next token. The re2c part of lex() finds the txt span covering the
 * next token, then lex_return() finds the corresponding line numbers
 * with help from count_lines().
 */
typedef struct Position {
	span name;
	span src;
	Token tok;
	span txt;
	Lino lino, linext;
} Position;

/*
 * Start off lexing with a fake newline token, so that it's easier to tell
 * if a preprocessor directive is at the start of a line without special
 * cases for the start of a file. The spelling of the fake newline is
 * usually empty so it doesn't cause a spurious newline appear in the output.
 *
 * C++23 specifies that a byte order mark at the start of a file is
 * ignored. We include it in the spelling of the fake newline so the
 * special case is imprisoned here. We assume there is a '\0' sentinel.
 *
 * The lexer assumes that the next character after the current token txt is
 * not part of a backslash-newline sequence. When lex() calls lex_return(),
 * the cursor is pointing at a lookahead byte that is definitely not part
 * of the new token. To find that byte, the lexer has skipped any trailing
 * backslash-newline. So usually, backslash-newline gets treated as part of
 * the preceding token, so the lexer's starting assumption is true. When
 * the source starts with a backslash-newline, we need to treat it as part
 * of the fake newline for the same reason.
 */
static Position
lex_start(span name, span src) {
	const char *utf8_bom = "\xEF\xBB\xBF";
	const size_t bom_len = 3;
	const byte *after_bom = src.ptr +
		(strncmp((SC)name.ptr, utf8_bom, bom_len) == 0 ? bom_len : 0);
	span txt = (span){ src.ptr, backslash_newline(after_bom) };
	Position here = { .name = name,
			  .src = src,
			  .txt = txt,
			  .tok = NEWLINE,
			  .lino = 1,
			  .linext = 1 + count_lines(txt) };
	return(here);
}

/*
 * WG14 5.1.1.2		Translation phases
 * WG21 [lex.phases]	Phases of translation
 *
 * This implements phase 1 newline recognition and phase 2
 * backslash-newline omission to help later phases consume
 * "wire-format" source directly. Assumes a '\0' sentinel.
 *
 * GCC allows but warns about space between backslash and newline.
 */
static const byte *
backslash_newline(const byte *p) {
	for(;;) {
		/* Microsoft backslash-crlf */
		if(p[0] == '\\' && p[1] == '\r' && p[2] == '\n') {
			p += 3;
			continue;
		}
		/* Acorn / Apple backslash-return */
		if(p[0] == '\\' && p[1] == '\r') {
			p += 2;
			continue;
		}
		/* Unix backslash-newline */
		if(p[0] == '\\' && p[1] == '\n') {
			p += 2;
			continue;
		}
		return(p);
	}
}

/*
 * It is a bit easier to rescan for line numbers than keep track
 * during lexing, so that we don't have to plumb the counter
 * through backslash_newline() which usually prefers not to care.
 */
static Lino
count_lines(span s) {
	const byte *p = s.ptr;
	Lino n = 0;
	while(p < s.end) {
		/* Microsoft backslash-crlf */
		if(p[0] == '\r' && p[1] == '\n') {
			n += 1; p += 2;
			continue;
		}
		/* Acorn / Apple backslash-return */
		/* Unix backslash-newline */
		if(p[0] == '\r' || p[0] == '\n') {
			n += 1; p += 1;
			continue;
		}
		p += 1;
		continue;
	}
	return(n);
}

/*
 * WG14 6.4.7		Header names
 * WG14 6.10.2		Source file inclusion
 * WG21 [lex.header]	Header names
 * WG21 [cpp.include]	Source file inclusion
 *
 * We can't use normal lexical rules when parsing #include lines. Header
 * names are identified in phase 3 so backslash-newline processing still
 * applies. And we need to recognise comments before and after header names
 * correctly.
 *
 * This should be called when the lookahead token is K_include. It
 * will eat any whitespace and comments and return the next token,
 * which is normally a HEADER but might not be if the #include expects
 * macro expansion or is malformed.
 */
static Token
header_name(Position *here) {
	const byte *cursor;
	for(;;) {
		cursor = backslash_newline(here->txt.end);
		if(*cursor == '<' || *cursor == '"')
			break;
		Token tok = lex(here);
		// non-header-name subject to macro expansion
		if(!spacial(tok))
			return(tok);
	}
	const byte delimiter = *cursor == '"' ? '"' : '>';
	for(;;) {
		cursor = backslash_newline(cursor+1);
		// fall back to non-header-name if we overrun
		if(*cursor == '\r' || *cursor == '\n' ||
		   cursor >= here->src.end)
			return(lex(here));
		if(*cursor == delimiter)
			break;
	}
	return(lex_return(here, HEADER, cursor+1));
}

/*
 * WG21 [lex.pptoken]	Preprocessing tokens
 * WG21 [lex.string]	String literals
 *
 * Raw strings are not even context-free, let alone regular, so they
 * need a bit of help to lex. We have a token spanning the opening
 * delimiter of the string and we need to find the closing delimiter.
 * The assertions should be guaranteed by the lexer. Line numbers are
 * handled when the lexer returns.
 *
 * The delimiter we are handed by the lexer can include backslash
 * newline sequences (because it is oblivious to them) which is not
 * allowed. The standards say phase 1 and phase 2 are reverted (!!!)
 * before d-chars are identified, and d-chars must not contain
 * backslash.
 *
 * Like Clang and GCC, we don't include user-defined literal suffixes in
 * the token. For the reasons discussed above lex_start(), we must
 * include any trailing backslash-newline sequence.
 */
static Token
raw_string(Position *here, const byte *cursor) {
	const byte *start = here->txt.end; // of the previous token
	const byte *limit = here->src.end;
	assert(start < cursor && cursor <= limit);
	size_t curlen = (size_t)(cursor - start);
	const byte *qq = memchr(start, '"', curlen);
	const byte *lp = memchr(start, '(', curlen);
	assert(qq != NULL && lp != NULL && lp > qq);
	const byte *ds = qq + 1; // delimiter string
	size_t dlen = (size_t)(lp - ds);
	// in case of backslash-newline shenanigans
	if(memchr(ds, '\\', dlen) != NULL) goto error;
	// enough space for right delimiter )dlen"
	const byte *dlimit = limit - dlen - 2;
	const byte *rp = lp; // right paren / left paren
	for(;;) {
		if(rp > dlimit) goto error;
		size_t rlen = (size_t)(dlimit - rp);
		rp = memchr(rp, ')', rlen);
		if(rp == NULL) goto error; else rp++;
		if(memcmp(rp, ds, dlen) == 0 && rp[dlen] == '"')
			return(lex_return(here, STRING,
				backslash_newline(rp + dlen + 1)));
	}
error:	// If it is malformed, head towards the undefined behaviour
	// category of mismatched " by returning the encoding prefix as an
	// identifier (which will at least be an R) and soldiering on.
	return(lex_return(here, IDENT, qq));
}

/*
 * Update our position with the results of lexical analysis.
 */
static Token
lex_return(Position *here, Token tok, const byte *cursor) {
	here->tok = tok;
	here->txt.ptr = here->txt.end;
	here->txt.end = cursor;
	here->lino = here->linext;
	here->linext += count_lines(here->txt);
	if(dflag('L')) token_debug(here);
	return(tok);
}

/*           ___
 *   _ _ ___|_  )__
 *  | '_/ -_)/ // _|  Now entering re2c territory ...
 *  |_| \___/___\__|
 *
 * We're using re2c's generic/custom API for accessing its input so
 * that we can cope with possible backslash-newline between any
 * characters. The macros below are the same as the example in re2c's
 * documentation showing the how to implement the default API using the
 * generic API, apart from the call to backslash_newline().
 *
 * YYFILL is disabled because all of the lexer's input is in memory.
 *
 * The EOF sentinel is '\0'; setting it means we don't need to allow
 * for overscan, but '\0' still passes through the lexer OK.
 */
/*!re2c	re2c:flags:input = custom;
	re2c:yyfill:enable = 0;
	re2c:eof = 0;
*/

#define YYCTYPE		byte
#define YYPEEK()	*YYCURSOR
#define YYSKIP()	(YYCURSOR = backslash_newline(YYCURSOR + 1))
#define YYLESSTHAN(n)	(YYLIMIT - YYCURSOR < n)
#define YYBACKUP()	(YYMARKER = YYCURSOR)
#define YYRESTORE()	(YYCURSOR = YYMARKER)

#define lexed(tok)	return(lex_return(here, tok, YYCURSOR))

static Token
lex(Position *here) {
	const byte *YYCURSOR = here->txt.end;
	const byte *YYLIMIT  = here->src.end;
	const byte *YYMARKER = NULL;
	/*
	 * re2c does not check there are characters available before starting,
	 * because it assumes/requires there is a '\0' sentinel at *YYLIMIT.
	 * Sometimes we're lexing a partial span, when the end is before the
	 * sentinel, so we need an extra check to make sure re2c does not scan
	 * more than we want. Our partial spans are aligned to token
	 * boundaries so we can just check before starting.
	 */
	if(YYLESSTHAN(1))		{ lexed(NOMORE);		}
	/*
	 * Extra token names used elsewhere: HEADER has its own special
	 * lexer, and MARKER is used during macro expansion to stop
	 * PASTE reaching past empty macro arguments.
	 */
	if(0)				{ lexed(HEADER);		}
	if(0)				{ lexed(MARKER);		}
%{
hack_to_stop_emacs_indenting = [}}];

// The comments here are stripped by re2c, so they generally just
// talk about how the regular expressions relate to the standards.
// The whys and wherefores should be explained in the commentar
// about token types above.

// WG14 5.1.1.2		Translation phases
// WG21 [lex.phases]	Phases of translation
//
// In C a missing newline at EOF is an error.
// In C++ a missing newline at EOF is appended.
// We just ignore the issue.

$					{ lexed(NOMORE);		}

// WG14 5.1.1.2		Translation phases
// WG14 J.3.2		Implementation-defined behavior
// WG21 [lex.phases]	Phases of translation
//
// Phase 1 mapping input characters to newline

'\r\n' | [\r\n]				{ lexed(NEWLINE);		}

// WG14 6.4p3		white-space characters
// WG14 J.3.1		Implementation-defined behavior
// WG21 [lex.pptoken]	Preprocessing tokens
//
// (space, horizontal tab, newline, vertical tab, and form feed)
//
// Whitespace characters are preserved not collapsed.

// WG14 6.10p5		white-space characters
// WG21 [cpp.pre]p4	white-space characters
//
// We ignore the constraint that the only whitespace characters
// allowed between tokens in a preprocessing directive are space
// and horizontal tab.

[ \t\v\f]+				{ lexed(SPACE);			}

// WG14 6.4p3		categories of preprocessing token
// WG21 [lex.pptoken]	Preprocessing tokens
//
// Single non-whitespace characters that do not lexically match the
// other preprocessing token categories, via the re2c default rule.

*					{ lexed(OTHER);			}
[\x00-\x1F\x7F]				{ lexed(CTRL);			}

// WG14 6.4.9		Comments
// WG21 [lex.comment]	Comments
//
// C++ restricts where [\v\f] can appear in // comments.
// Like C, we don't care.

"//"[^\r\n]*				{ lexed(COMMENT);		}

"/*"([^*]*[*]+[^*/])*[^*]*[*]+[/]	{ lexed(COMMENT);		}

// WG14 6.4.6		Punctuators
// WG21 [lex.operators]	Operators and punctuators
//
// Pruned as explained above.

'#'					{ lexed(HASH);			}
'##'					{ lexed(PASTE);			}
'...'					{ lexed(REST);			}

// numbers on the right are operator precedence / binding power
// negative means it's not a binary operator or not implemented
// an explicit positive means it has standard binary implementation
// assignment is only used in -Dsym=val options

'('					{ lexed(OPEN);			} //  32
')'					{ lexed(CLOSE);			} // -32
'--'					{ lexed(DEC);			} // -30
'++'					{ lexed(INC);			} // -30
'~'					{ lexed(COMPL);			} // -28
'!'					{ lexed(NOT);			} // -28
'/'					{ lexed(DIV);			} // +26
'%'					{ lexed(MOD);			} // +26
'*'					{ lexed(MUL);			} // +26
'+'					{ lexed(ADD);			} // +24
'-'					{ lexed(SUB);			} // +24
'<<'					{ lexed(SHL);			} //  22
'>>'					{ lexed(SHR);			} //  22
'>='					{ lexed(GE);			} // +20
'>'					{ lexed(GT);			} // +20
'<='					{ lexed(LE);			} // +20
'<'					{ lexed(LT);			} // +20
'=='					{ lexed(EQ);			} // +18
'!='					{ lexed(NOT_EQ);		} // +18
'&'					{ lexed(BITAND);		} // +16
'^'					{ lexed(XOR);			} // +14
'|'					{ lexed(BITOR);			} // +12
'&&'					{ lexed(AND);			} //  10
'||'					{ lexed(OR);			} //   8
'?'					{ lexed(QUERY);			} //   6
':'					{ lexed(COLON);			} //  -5
'='					{ lexed(ASSIGN);		} //  -4
','					{ lexed(COMMA);			} //  -2

// WG14 6.4.6p3*	digraphs
// WG14 7.9		Alternative spellings
// WG14 6.10		Preprocessing directives
// WG21 [lex.digraph]	Alternative tokens
// WG21 [cpp.pre]	Preprocessing directives
//
// In C the wordy tokens are defined in <iso646.h>,
// but in C++ they are built in.
// They are a subset of identifiers, so they need to come first so
// they win according to re2c priority order.

"%:"					{ lexed(HASH);			}
"%:%:"					{ lexed(PASTE);			}

// (precedence and binding power values are maintained manually)
// Keywords for directives and operators maintained automatically

"__VA_ARGS__"				{ lexed(K_va_args);		} // -32
"__VA_OPT__"				{ lexed(K_va_opt);		} // -32
"defined"				{ lexed(K_defined);		} // -30
"false"					{ lexed(K_false);		} // -30
"true"					{ lexed(K_true);		} // -30
"compl"					{ lexed(K_compl);		} // -28
"not"					{ lexed(K_not);			} // -28
"not_eq"				{ lexed(K_not_eq);		} // +18
"bitand"				{ lexed(K_bitand);		} // +16
"xor"					{ lexed(K_xor);			} // +14
"bitor"					{ lexed(K_bitor);		} // +12
"and"					{ lexed(K_and);			} // +10
"or"					{ lexed(K_or);			} // +8
"define"				{ lexed(K_define);		}
"defval"				{ lexed(K_defval);		}
"elif"					{ lexed(K_elif);		}
"elifdef"				{ lexed(K_elifdef);		}
"elifndef"				{ lexed(K_elifndef);		}
"else"					{ lexed(K_else);		}
"endif"					{ lexed(K_endif);		}
"if"					{ lexed(K_if);			}
"ifdef"					{ lexed(K_ifdef);		}
"ifndef"				{ lexed(K_ifndef);		}
"import"				{ lexed(K_import);		}
"include"				{ lexed(K_include);		}
"include_next"				{ lexed(K_include_next);	}
"line"					{ lexed(K_line);		}
"nodefines"				{ lexed(K_nodefines);		}
"noincludes"				{ lexed(K_noincludes);		}
"pragma"				{ lexed(K_pragma);		}
"undef"					{ lexed(K_undef);		}
"unifdef"				{ lexed(K_unifdef);		}

// end of maintained section

// WG14 6.4.3		Universal character names
// WG21 [lex.phases]	Phases of translation
// WG21 [lex.charset]	Character sets
//
// C++23 named universal characters. The name can contain any
// character in the source character set apart from } or newline.

n_char	= [\x20-\x7C\x7E\x80-\xFF]					;

hex	= [0-9a-fA-F]							;

ucn4	= "\\u" hex{4}							;
ucn8	= "\\U" hex{8}							;
ucnu	= "\\u{" hex+ "}"						;
ucnN	= "\\N{" n_char+ "}"						;
ucn	= ucn4 | ucn8 | ucnu | ucnN					;

// WG14 6.4.2		Identifiers
// WG14 D		Universal character names for identifiers
// WG14 J.3.3		Implementation-defined behavior
// WG21 [lex.name]	Identifiers
//
// C allows other implementation-defined characters.
// C++ allows non-ASCII characters that were converted to
// universal character names in phase 1.
// Old unifdef supported the common subset.
// We allow '$', as is traditional in C, and any top-bit-set byte and
// any universal character name as a superset of the characters
// permitted by C++.

letter	= [a-zA-Z_$\x80-\xFF]						;
idchar	=  letter | ucn | [0-9]						;
ident	= (letter | ucn) idchar*					;

ident					{ lexed(IDENT);			}

// WG14 6.4.4.1		Integer constants
// WG21 [lex.icon]	Integer literals
//
// These are a subset of the preprocessor numbers, so they need
// to come first so they win according to re2c priority order.
//
// Binary integer constants are new in C++14 and C23.
// We can't evaluate integers with ' digit separators.
//
// The token type encodes whether there is a prefix, what the base is,
// what signedness the constant has, and whether it is LL. Bare digit
// sequences are handled as special cases for #line directives.

llong	= ("ll"|"LL")							;
ullong	= llong[uU]|[uU]|[uU]llong					;
unsign	=  [lL][uU]|[uU]|[uU][lL]					;

[0][0-7]*				{ lexed(ODIGITS);		}
[1-9][0-9]*				{ lexed(DIGITS);		}

[0][bB][01]+ [lL]?			{ lexed(INTb2s);		}
[0][bB][01]+ unsign			{ lexed(INTb2u);		}
[0][bB][01]+ llong			{ lexed(INTb2S);		}
[0][bB][01]+ ullong			{ lexed(INTb2U);		}
[0][0-7]* [lL]				{ lexed(INT_8s);		}
[0][0-7]* unsign			{ lexed(INT_8u);		}
[0][0-7]* llong				{ lexed(INT_8S);		}
[0][0-7]* ullong			{ lexed(INT_8U);		}
[1-9][0-9]* [lL]			{ lexed(INT_10s);		}
[1-9][0-9]* unsign			{ lexed(INT_10u);		}
[1-9][0-9]* llong			{ lexed(INT_10S);		}
[1-9][0-9]* ullong			{ lexed(INT_10U);		}
[0][xX]hex+ [lL]?			{ lexed(INTx16s);		}
[0][xX]hex+ unsign			{ lexed(INTx16u);		}
[0][xX]hex+ llong			{ lexed(INTx16S);		}
[0][xX]hex+ ullong			{ lexed(INTx16U);		}

// Integers that we cannot yet evaluate, which should be treated as an
// evaluation error rather than a syntax error.

ulength	= [lL] | unsign	| llong | ullong				;

[0][bB][01]+("'" [01]+)+ ulength?	{ lexed(PPINT);			}
[0][0-7]*   ("'"[0-7]+)+ ulength?	{ lexed(PPINT);			}
[1-9][0-9]* ("'"[0-9]+)+ ulength?	{ lexed(PPINT);			}
[0][xX]hex+ ("'" hex +)+ ulength?	{ lexed(PPINT);			}

// WG14 6.4.8		Preprocessing numbers
// WG21 [lex.ppnumber]	Preprocessing numbers
// WG21 [lex.ext]	User-defined literals
//
// C++14 and C23 allow ' as a digit separator.
// PP-numbers cover C++11 user-defined numeric literals.

[.]?[0-9]( ident | [eEpP][+-] | [.'] )*	{ lexed(PPNUM);			} // '

// WG14 6.4.4.4		Character constants
// WG14 7.19		Common definitions <stddef.h> whar_t
// WG14 7.28		Unicode utilities <uchar.h>
// WG21 [lex.ccon]	Character literals
//
// These are the character constants that we know how to evaluate, which
// are a subset of all possible character constants, so they need to come
// first so they win according to re2c priority order. The manual has a
// summary of what is supported and how it relates to the standards.
//
// The token type encodes which evaluation function to call, what
// signedness the constant has, when to sign-extend, and whether the
// fuzzer should filter it out.

esc	= [\\]('"'|"'"|[\\?abefnrtv])					;

// Everyone's favourite trivia: 8 and 9 are not octal digits.

oct7	= [\\][0-1]?[0-7]?[0-7]						;
oct8	= [\\][0-3]?[0-7]?[0-7]						;
oct9	= [\\][0-7]?[0-7]?[0-7]						;

hex2	= [\\][x]hex{1,2}						;
hex4	= [\\][x]hex{1,4}						;
hex8	= [\\][x]hex{1,8}						;
hexX	= [\\][x]hex{1,}						;

topbit	= [\x80-\xFF]							;
ascii	= [\x20-\x7E] \ [\\']						; // '
utfcont	= [\x80-\xBF]							;
utf2	= [\xC0-\xDF] utfcont						;
utf3	= [\xE0-\xEF] utfcont utfcont					;
utf4	= [\xF0-\xF7] utfcont utfcont utfcont				;

"L"?['] esc  [']			{ lexed(CH_esc_s);		}
[uU]['] esc  [']			{ lexed(CH_esc_u);		}
"u8"['] esc  [']			{ lexed(CH_esc_uZ);		}

    ['] oct8 [']			{ lexed(CH_oct_x);		}
"L" ['] oct9 [']			{ lexed(CH_oct_s);		}
[uU]['] oct9 [']			{ lexed(CH_oct_u);		}
"u8"['] oct7 [']			{ lexed(CH_oct_uZ);		}

    ['] hex2 [']			{ lexed(CH_hex_x);		}
    ['] ucn  [']			{ lexed(CH_hex_sZ);		}
"L" ['] hex8 [']			{ lexed(CH_hex_s);		}
"L" ['] ucn  [']			{ lexed(CH_hex_sZ);		}
"u" ['] hex4 [']			{ lexed(CH_hex_u);		}
"u" ['] ucn4 [']			{ lexed(CH_hex_uZ);		}
"U" ['] hex8 [']			{ lexed(CH_hex_u);		}
"U" ['] ucn  [']			{ lexed(CH_hex_uZ);		}

"u'\\U"  [0]{4} hex{4} "'"		{ lexed(CH_hex_uZ);		}
"u8'\\x"        [0-7]? hex "'"		{ lexed(CH_hex_uZ);		}
"u8'\\u" [0]{2} [0-7]  hex "'"		{ lexed(CH_hex_uZ);		}
"u8'\\U" [0]{6} [0-7]  hex "'"		{ lexed(CH_hex_uZ);		}

"L"?['] ascii [']			{ lexed(CH_ascii_s);		}
[uU]['] ascii [']			{ lexed(CH_ascii_u);		}
"u8"['] ascii [']			{ lexed(CH_ascii_uZ);		}
"L"?['] topbit [']			{ lexed(CH_byte_xZ);		}
[uU]['] topbit [']			{ lexed(CH_byte_uZ);		}
"L"?['] utf2 [']			{ lexed(CH_utf2_sZ);		}
[uU]['] utf2 [']			{ lexed(CH_utf2_uZ);		}
"L"?['] utf3 [']			{ lexed(CH_utf3_sZ);		}
[uU]['] utf3 [']			{ lexed(CH_utf3_uZ);		}
"L"?['] utf4 [']			{ lexed(CH_utf4_sZ);		}
[U] ['] utf4 [']			{ lexed(CH_utf4_uZ);		}

// WG14 6.4.4.4		Character constants
// WG14 6.4.5		String literals
// WG21 [lex.ccon]	Character literals
// WG21 [lex.string]	String literals
// WG21 [lex.ext]	User-defined literals
//
// C11 and C++11 added [uU]
// C11 and C++11 added u8 for strings
// C2x and C++14 added u8 for chacracters
// C++11 added user-defined literals
// (we treat a ud-suffix as a separate token like GCC and Clang)
//
// Simple strings are required in #line directives.

enc	= ("u8" | [uUL])?						;
seq	= esc | oct9 | hexX | ucn					;

    '"' (seq | [^"\\\r\n])* ["]		{ lexed(SSTRING);		}
enc '"' (seq | [^"\\\r\n])* ["]		{ lexed(STRING);		}
enc "'" (seq | [^'\\\r\n])+ [']		{ lexed(CHARACTER);		}

// WG21 [lex.charset]	Character sets
//
// C++11 added raw string literals. The delimiter can use any graphic
// character in the basic source character set apart from (\)

d_char	= [a-zA-Z0-9!#%&*+,./:;<=>?^_{|}~-] |'['|']'|'"'|"'"		;

enc [R]'"' d_char{0,16} '('	{ return(raw_string(here, YYCURSOR));	}

// That's all, folks!
%}
}

/***********************************************************************
 *      _ _               _      _      _        _    _
 *   __| (_)____ __  __ _| |_ __| |_   | |_ __ _| |__| |___   mainly
 *  / _` | (_-< '_ \/ _` |  _/ _| ' \  |  _/ _` | '_ \ / -_)  for the
 *  \__,_|_/__/ .__/\__,_|\__\__|_||_|  \__\__,_|_.__/_\___|  simplifier
 *            |_|
 */

/*
 * WG14 6.6		Constant expressions
 * WG14 6.10.1		Conditional inclusion
 * WG21 [cpp.cond]	Conditional inclusion
 *
 * Dispatch table for token-related functions.
 *
------------------------------------------------------------------------
 *
 * Much of this is for a Pratt-flavoured top-down operator precedence
 * parser / expression evaluator for #if expressions.
 *
 * Binary operators have a left binding power (lbp), i.e. their precedence.
 * Most operators are left-associative, in which case their right binding
 * power is the same as their left binding power. For right-associative
 * operators (which for us is only ?:) the right binding power is one less
 * than the left binding power. Prefix operators have a fixed, hard-coded
 * right binding power: see UNARY_NUD() below.
 *
 * A "null denotation" (nud) evaluates a token without any left context.
 * It's used for primary expressions (constants, symbols) and unary
 * expressions.
 *
 * A "left denotation" (led) evaluates a token with some left context, for
 * binary operators, ternary operators, function calls, etc.
 *
 * The tub function says what is the type of the result of the operator,
 * and what kind of undefined behaviour (if any) it has. In simple cases
 * it's just the usual arithmetic conversions.
 *
 * Binary operators have distinct signed and unsigned implementations,
 * selected by the type returned from their tub function. The bins and
 * binu function names match the operator names.
 *
 * The operators that we need to implement are determined by the rules for
 * #if expressions, which are integer constant expressions maybe with
 * defined() operators. The C standard is a lot more straightfoward than
 * C++ about what is allowed...
 *
 * Which tokens use nud_unknown()? In an expression, after macro
 * expansion, there can still be symbolic tokens, so most keywords and
 * other identifiers are unknown. The binary iso646.h operators are
 * treated as operators in operator position but as symbols in operand
 * position.
 *
 * Macro expansion handles defined() operators for known symbols, but
 * the expanded string can still have defined() operators for unknown
 * symbols. So defined() operators are parsed and given an unknown
 * value.
 *
 * Macro expansion can also leave behind invocations of unknown function-like
 * macros and __has_ tests, so the parser needs to recognise function calls
 * and treat them as unknown.
 *
 * Numbers can have known values (from CH and INT tokens), or if we
 * can't evaluate them (CHARACTER or PPINT) we treat them as a value
 * error; otherwise, floating point numbers and user-defined literal
 * numbers are treated as syntax errors, same as junk preprocessor
 * numbers.
 *
------------------------------------------------------------------------
 *
 * The other important purpose of the table is dispatching if-section
 * preprocessor directives, as part of the if-section state machine.
 *
 * The first (non-space, non-comment) token after a hash is dispatched
 * to a function that also depends on the current IfState, via an array
 * of `hash_fn` pointers.
 *
 * A `hash` member of the token table points to the first element of a
 * Directif array, which is a hash_fn. We don't use the Directif type in
 * this case because we want `hash` to point to an array of function
 * pointers, not to be an array nor point to an array of arrays. (One of
 * the odd cases in C where an array does not decay to a pointer to its
 * elements.)
 *
 * The Directif arrays are explained in more detail in the "preprocess"
 * section below.
 *
------------------------------------------------------------------------
 *
 * Some parts of the table are maintained automatically:
 *
 * - the list of tokens, extracted from lexer lexed() statements
 * - the operator binding powers, copied from comments in the lexer
 * - the character and integer nud functions
 * - which operators have the standard led_bin function
 * - all the bin functions
 * - all the show functions
 *
 * The other parts are maintained by hand.
 *
 * There are some rules governing what must appear in the table so that the
 * eval(), led_bin(), and keep_group()/drop_group() functions do not crash.
 *
 * - Every token must have a nud function, to do something useful or to
 *   handle a syntax error.
 *
 * - Every token with a non-zero left binding power must have a led function.
 *   (led functions are otherwise unused)
 *
 * - Every token that uses led_bin() must have tub and bin entries.
 *   (tub and bin functions are otherwise unused)
 *
 * - Every token must have keep/drop functions, to copy a line to the output
 *   or rewrite or delete it.
 *
 * - Every token has a show function for pretty-printing in debug or error
 *   handling code, defined in the next section.
 *
 * These rules are checked by the automaint script.
 */

#define led_NULL NULL
#define bins_NULL NULL
#define binu_NULL NULL

static const struct {
	int lbp;
	nud_fn *nud;
	led_fn *led;
	tub_fn *tub;
	bins_fn *bins;
	binu_fn *binu;
	hash_fn **hash;
	show_fn *show;
} token[] = {

#define TOKEN(lbp, N, L, tub, B, H, S) \
{ lbp, nud_##N, led_##L, tub, bins_##B, binu_##B, hash_##H, show_##S }

// dispatch table maintained semi-automatically
TOKEN(0, error,     NULL, NULL,      NULL,  non,  COMMENT  ),// COMMENT
TOKEN(0, error,     NULL, NULL,      NULL,  non,  CTRL     ),// CTRL
TOKEN(0, error,     NULL, NULL,      NULL,  non,  HASH     ),// HASH
TOKEN(0, error,     NULL, NULL,      NULL,  non,  HEADER   ),// HEADER
TOKEN(0, unknown,   NULL, NULL,      NULL,  non,  IDENT    ),// IDENT
TOKEN(0, error,     NULL, NULL,      NULL,  non,  MARKER   ),// MARKER
TOKEN(0, nomore,    NULL, NULL,      NULL,  non,  NEWLINE  ),// NEWLINE
TOKEN(0, nomore,    NULL, NULL,      NULL,  non,  NOMORE   ),// NOMORE
TOKEN(0, error,     NULL, NULL,      NULL,  non,  OTHER    ),// OTHER
TOKEN(0, novalue,   NULL, NULL,      NULL,  non,  PPINT    ),// PPINT

TOKEN(0, error,     NULL, NULL,      NULL,  non,  PPNUM    ),// PPNUM
TOKEN(0, error,     NULL, NULL,      NULL,  non,  REST     ),// REST
TOKEN(0, error,     NULL, NULL,      NULL,  non,  SPACE    ),// SPACE
TOKEN(0, error,     NULL, NULL,      NULL,  non,  STRING   ),// SSTRING
TOKEN(0, error,     NULL, NULL,      NULL,  non,  STRING   ),// STRING
TOKEN(0, novalue,   NULL, NULL,      NULL,  non,  character),// CHARACTER
TOKEN(0, CH_ascii_s,NULL, NULL,      NULL,  non,  character),// CH_ascii_s
TOKEN(0, CH_ascii_u,NULL, NULL,      NULL,  non,  character),// CH_ascii_u
TOKEN(0, CH_ascii_u,NULL, NULL,      NULL,  non,  character),// CH_ascii_uZ
TOKEN(0, CH_byte_u, NULL, NULL,      NULL,  non,  character),// CH_byte_uZ

TOKEN(0, CH_byte_x, NULL, NULL,      NULL,  non,  character),// CH_byte_xZ
TOKEN(0, CH_esc_s,  NULL, NULL,      NULL,  non,  character),// CH_esc_s
TOKEN(0, CH_esc_u,  NULL, NULL,      NULL,  non,  character),// CH_esc_u
TOKEN(0, CH_esc_u,  NULL, NULL,      NULL,  non,  character),// CH_esc_uZ
TOKEN(0, CH_hex_s,  NULL, NULL,      NULL,  non,  character),// CH_hex_s
TOKEN(0, CH_hex_s,  NULL, NULL,      NULL,  non,  character),// CH_hex_sZ
TOKEN(0, CH_hex_u,  NULL, NULL,      NULL,  non,  character),// CH_hex_u
TOKEN(0, CH_hex_u,  NULL, NULL,      NULL,  non,  character),// CH_hex_uZ
TOKEN(0, CH_hex_x,  NULL, NULL,      NULL,  non,  character),// CH_hex_x
TOKEN(0, CH_oct_s,  NULL, NULL,      NULL,  non,  character),// CH_oct_s

TOKEN(0, CH_oct_u,  NULL, NULL,      NULL,  non,  character),// CH_oct_u
TOKEN(0, CH_oct_u,  NULL, NULL,      NULL,  non,  character),// CH_oct_uZ
TOKEN(0, CH_oct_x,  NULL, NULL,      NULL,  non,  character),// CH_oct_x
TOKEN(0, CH_utf2_s, NULL, NULL,      NULL,  non,  character),// CH_utf2_sZ
TOKEN(0, CH_utf2_u, NULL, NULL,      NULL,  non,  character),// CH_utf2_uZ
TOKEN(0, CH_utf3_s, NULL, NULL,      NULL,  non,  character),// CH_utf3_sZ
TOKEN(0, CH_utf3_u, NULL, NULL,      NULL,  non,  character),// CH_utf3_uZ
TOKEN(0, CH_utf4_s, NULL, NULL,      NULL,  non,  character),// CH_utf4_sZ
TOKEN(0, CH_utf4_u, NULL, NULL,      NULL,  non,  character),// CH_utf4_uZ
TOKEN(0, INT_8s,    NULL, NULL,      NULL,  non,  integer  ),// ODIGITS

TOKEN(0, INT_10s,   NULL, NULL,      NULL,  non,  integer  ),// DIGITS
TOKEN(0, INTb2s,    NULL, NULL,      NULL,  non,  integer  ),// INTb2S
TOKEN(0, INTb2u,    NULL, NULL,      NULL,  non,  integer  ),// INTb2U
TOKEN(0, INTb2s,    NULL, NULL,      NULL,  non,  integer  ),// INTb2s
TOKEN(0, INTb2u,    NULL, NULL,      NULL,  non,  integer  ),// INTb2u
TOKEN(0, INT_8s,    NULL, NULL,      NULL,  non,  integer  ),// INT_8S
TOKEN(0, INT_8u,    NULL, NULL,      NULL,  non,  integer  ),// INT_8U
TOKEN(0, INT_8s,    NULL, NULL,      NULL,  non,  integer  ),// INT_8s
TOKEN(0, INT_8u,    NULL, NULL,      NULL,  non,  integer  ),// INT_8u
TOKEN(0, INT_10s,   NULL, NULL,      NULL,  non,  integer  ),// INT_10S

TOKEN(0, INT_10u,   NULL, NULL,      NULL,  non,  integer  ),// INT_10U
TOKEN(0, INT_10s,   NULL, NULL,      NULL,  non,  integer  ),// INT_10s
TOKEN(0, INT_10u,   NULL, NULL,      NULL,  non,  integer  ),// INT_10u
TOKEN(0, INTx16s,   NULL, NULL,      NULL,  non,  integer  ),// INTx16S
TOKEN(0, INTx16u,   NULL, NULL,      NULL,  non,  integer  ),// INTx16U
TOKEN(0, INTx16s,   NULL, NULL,      NULL,  non,  integer  ),// INTx16s
TOKEN(0, INTx16u,   NULL, NULL,      NULL,  non,  integer  ),// INTx16u
TOKEN(0, unknown,   NULL, NULL,      NULL,  non,  K_va_args),// K_va_args
TOKEN(0, unknown,   NULL, NULL,      NULL,  non,  K_va_opt ),// K_va_opt
TOKEN(0, defined,   NULL, NULL,      NULL,  non,  K_defined),// K_defined

TOKEN(0, FALSE,     NULL, NULL,      NULL,  non,  keyword  ),// K_false
TOKEN(0, TRUE,      NULL, NULL,      NULL,  non,  keyword  ),// K_true
TOKEN(0, COMPL,     NULL, NULL,      NULL,  non,  keyword  ),// K_compl
TOKEN(0, NOT,       NULL, NULL,      NULL,  non,  keyword  ),// K_not
TOKEN(18,unknown,   bin,  tub_cmp,   NOT_EQ,non,  keyword  ),// K_not_eq
TOKEN(16,unknown,   bin,  U_A_C,     BITAND,non,  keyword  ),// K_bitand
TOKEN(14,unknown,   bin,  U_A_C,     XOR,   non,  keyword  ),// K_xor
TOKEN(12,unknown,   bin,  U_A_C,     BITOR, non,  keyword  ),// K_bitor
TOKEN(10,unknown,   AND,  NULL,      NULL,  non,  keyword  ),// K_and
TOKEN(8, unknown,   OR,   NULL,      NULL,  non,  keyword  ),// K_or

TOKEN(0, unknown,   NULL, NULL,      NULL,  def,  keyword  ),// K_define
TOKEN(0, unknown,   NULL, NULL,      NULL,  def,  keyword  ),// K_defval
TOKEN(0, unknown,   NULL, NULL,      NULL,  ELIF, keyword  ),// K_elif
TOKEN(0, unknown,   NULL, NULL,      NULL,  ELDEF,keyword  ),// K_elifdef
TOKEN(0, unknown,   NULL, NULL,      NULL,  ELDEF,keyword  ),// K_elifndef
TOKEN(0, unknown,   NULL, NULL,      NULL,  ELSE, keyword  ),// K_else
TOKEN(0, unknown,   NULL, NULL,      NULL,  ENDIF,keyword  ),// K_endif
TOKEN(0, unknown,   NULL, NULL,      NULL,  IF,   keyword  ),// K_if
TOKEN(0, unknown,   NULL, NULL,      NULL,  IFDEF,keyword  ),// K_ifdef
TOKEN(0, unknown,   NULL, NULL,      NULL,  IFDEF,keyword  ),// K_ifndef

TOKEN(0, unknown,   NULL, NULL,      NULL,  inc,  keyword  ),// K_import
TOKEN(0, unknown,   NULL, NULL,      NULL,  inc,  keyword  ),// K_include
TOKEN(0, unknown,   NULL, NULL,      NULL,  inc,  keyword  ),// K_include_next
TOKEN(0, unknown,   NULL, NULL,      NULL,  line, keyword  ),// K_line
TOKEN(0, unknown,   NULL, NULL,      NULL,  non,  keyword  ),// K_nodefines
TOKEN(0, unknown,   NULL, NULL,      NULL,  non,  keyword  ),// K_noincludes
TOKEN(0, unknown,   NULL, NULL,      NULL,  non,  keyword  ),// K_pragma
TOKEN(0, unknown,   NULL, NULL,      NULL,  def,  keyword  ),// K_undef
TOKEN(0, unknown,   NULL, NULL,      NULL,  non,  keyword  ),// K_unifdef
TOKEN(0, error,     NULL, NULL,      NULL,  non,  operator ),// CLOSE

TOKEN(32,OPEN,      OPEN, NULL,      NULL,  non,  operator ),// OPEN
TOKEN(0, error,     NULL, NULL,      NULL,  non,  operator ),// DEC
TOKEN(0, error,     NULL, NULL,      NULL,  non,  operator ),// INC
TOKEN(0, COMPL,     NULL, NULL,      NULL,  non,  operator ),// COMPL
TOKEN(0, NOT,       NULL, NULL,      NULL,  non,  operator ),// NOT
TOKEN(26,error,     bin,  undivined, DIV,   non,  operator ),// DIV
TOKEN(26,error,     bin,  undivined, MOD,   non,  operator ),// MOD
TOKEN(26,error,     bin,  maltiply,  MUL,   non,  operator ),// MUL
TOKEN(24,ADD,       bin,  attrition, ADD,   non,  operator ),// ADD
TOKEN(24,SUB,       bin,  submarine, SUB,   non,  operator ),// SUB

TOKEN(22,error,     bin,  too_shifty,SHL,   non,  operator ),// SHL
TOKEN(22,error,     bin,  too_shifty,SHR,   non,  operator ),// SHR
TOKEN(20,error,     bin,  tub_cmp,   GE,    non,  operator ),// GE
TOKEN(20,error,     bin,  tub_cmp,   GT,    non,  operator ),// GT
TOKEN(20,error,     bin,  tub_cmp,   LE,    non,  operator ),// LE
TOKEN(20,error,     bin,  tub_cmp,   LT,    non,  operator ),// LT
TOKEN(18,error,     bin,  tub_cmp,   EQ,    non,  operator ),// EQ
TOKEN(18,error,     bin,  tub_cmp,   NOT_EQ,non,  operator ),// NOT_EQ
TOKEN(16,error,     bin,  U_A_C,     BITAND,non,  operator ),// BITAND
TOKEN(14,error,     bin,  U_A_C,     XOR,   non,  operator ),// XOR

TOKEN(12,error,     bin,  U_A_C,     BITOR, non,  operator ),// BITOR
TOKEN(10,error,     AND,  NULL,      NULL,  non,  operator ),// AND
TOKEN(8, error,     OR,   NULL,      NULL,  non,  operator ),// OR
TOKEN(6, error,     QUERY,NULL,      NULL,  non,  operator ),// QUERY
TOKEN(0, error,     NULL, NULL,      NULL,  non,  operator ),// COLON
TOKEN(0, error,     NULL, NULL,      NULL,  non,  operator ),// ASSIGN
TOKEN(0, error,     NULL, NULL,      NULL,  non,  operator ),// COMMA
TOKEN(0, error,     NULL, NULL,      NULL,  non,  operator ),// PASTE
// end of maintained section

#undef TOKEN

};

#undef led_NULL
#undef bins_NULL
#undef binu_NULL

/***********************************************************************
 *                     _
 *   _ __  __ _ _ _ __(_)_ _  __ _   syntactic
 *  | '_ \/ _` | '_(_-< | ' \/ _` |  helper
 *  | .__/\__,_|_| /__/_|_||_\__, |  functions
 *  |_|                      |___/
 */

/*
 * WG14 6.10.8		 Predefined macro names
 * WG21 [cpp.predefined] Predefined macro names
 *
 * As well as printing tokens, the show function pointers can also
 * indicate a token's category. We use this to identify which tokens
 * can be symbols.
 *
 * The show functions themselves are used for diagnostics or
 * debugging, so they always print to stderr.
 */

#define SHOWER(name, ...) static SHOW(name) {				\
		(void)s; eprintf(__VA_ARGS__);				\
	} struct _ // allow trailing semicolon

SHOWER(COMMENT,		"a comment");
SHOWER(CTRL,		"some garbage '\\x%02x'", *(SC)s.ptr);
SHOWER(HASH,		"a hash");
SHOWER(HEADER,		"a header file name");
SHOWER(IDENT,		"an identifier %.*s", spanf(s));
SHOWER(NEWLINE,		"a newline");
SHOWER(NOMORE,		"end of file");
SHOWER(OTHER,		"some chaff '%c'", *(SC)s.ptr);
SHOWER(MARKER,		"an empty macro argument");
SHOWER(PPINT,		"an integer %.*s", spanf(s));
SHOWER(PPNUM,		"a number %.*s", spanf(s));
SHOWER(REST,		"variable parameters '...'");
SHOWER(SPACE,		"white space");
SHOWER(STRING,		"a string literal %.*s", spanf(s));

/*
 * These are not normal keywords because they can't be used as symbols.
 */
SHOWER(K_defined,	"a defined() operator");
SHOWER(K_va_args,	"%.*s", spanf(s));
SHOWER(K_va_opt,	"%.*s", spanf(s));

SHOWER(character,	"a character literal %.*s", spanf(s));
SHOWER(integer,		"an integer %.*s", spanf(s));
SHOWER(keyword,		"the %.*s keyword", spanf(s));
SHOWER(operator,	"a %.*s operator", spanf(s));

#undef SHOWER

// categories of tokens ------------------------------------------------

static bool
is_symbol(Token tok) {
	show_fn *show = token[tok].show;
	return(show == show_IDENT || show == show_keyword);
}

/*
 * Whitespace inside preprocessor directives.
 */
static bool
spacial(Token tok) {
	return(tok == SPACE || tok == COMMENT);
}

/*
 * Vertical and horizontal whitespace in program text, but not comments.
 */
static bool
very_spacial(Token tok) {
	return(tok == SPACE || tok == NEWLINE);
}

/*
 * Testing for NEWLINE directly is always a mistake.
 */
static bool
inside_line(Token tok) {
	return(tok != NEWLINE && tok != NOMORE);
}

// parser utilities ----------------------------------------------------

/*
 * WG14 5.1.1.2		Translation phases
 * WG21 [lex.phases]	Phases of translation
 *
 * Because the lexer preserves comments, we need to skip whitespace and
 * comments as if translation phase 3 had replaced the comments with
 * spaces. But there are some subtleties...
 */

/*
 * Because preprocessor directives are line-based, it's natural to
 * iterate over the file roughly line-by-line - roughly, because of
 * backslash-newline, multiline comments, and collapsing vertical space
 * around deleted preprocessor directives.
 *
 * This function is the usual condition for looping over a file, used
 * when we have processed a line and we want to find out if there is
 * more input to process. To avoid getting confused, we ensure we are
 * actually at a newline, then bump to the following lookahead token.
 */
static bool
more_lines(Position *here) {
	assert(!inside_line(here->tok));
	return(lex(here) != NOMORE);
}

/*
 * Skip whitespace and comments at the start of a line that might be a
 * preprocessor directive. This is used after more_lines() has done the
 * newline sanity check.
 *
 * A preprocessor directive owns its preceding blank vertical space (not
 * including comments) so that when we delete a group the surrounding
 * vertical space gets somewhat collapsed.
 *
 * A directive also owns any comments between the start of the (logical)
 * line and the # mark.
 */
static Token
first(Position *here) {
	Token tok = here->tok;
	while(very_spacial(tok)) tok = lex(here);
	while(spacial(tok))      tok = lex(here);
	return(tok);
}

/*
 * Skip past horizontal whitespace to the next token of interest. This
 * is for use within preprocessor lines, so it does not skip newlines.
 */
static Token
next(Position *here) {
	Token tok = here->tok;
	if(inside_line(tok)) tok = lex(here);
	while(spacial(tok))  tok = lex(here);
	return(tok);
}

/*
 * For lines that we don't want to look at in detail.
 */
static void
skip_to_newline(Position *here) {
	Token tok = here->tok;
	while(inside_line(tok))
		tok = lex(here);
}

// diagnostics ---------------------------------------------------------

static void
show_token(Position *here) {
	token[here->tok].show(here->txt);
}

/*
 * Called by lex_return() for tracing the lexer.
 */
static void
token_debug(Position *here) {
	if(diagnostic(here, "debug", 'L')) {
		eprintf("lexed ");
		show_token(here);
		putc('\n', stderr);
	}
}

/*
 * Messages from the parser.
 */

static bool
diagnostic(Position *here, const char *severity, char flag) {
	if(!dflag(flag)) return(false);
	eprintf("%.*s:%zu: %s(%c): ",
		spanf(here->name), here->lino, severity, (char)flag);
	return(true);
}

static void
parse_expected(Position *here, const char *expected, const char *recovery) {
	if(diagnostic(here, "error", 'x')) {
		eprintf("expected %s but found ", expected);
		show_token(here);
		eprintf("; %s\n", recovery);
	}
}

static void
parse_nothing_after(Position *here, const char *before) {
	Token tok = next(here);
	if(inside_line(tok) &&
	   diagnostic(here, "error", 'j')) {
		show_token(here);
		eprintf(" is not allowed after %s\n", before);
	}
	skip_to_newline(here);
}

static void
parse_unexpected(Position *here, const char *thing) {
	if(diagnostic(here, "error", 'j'))
		eprintf("unexpected %s", thing);
	skip_to_newline(here);
}

static void
inexpressible(Position *here, const char *expected) {
	if(diagnostic(here, "error", 'x')) {
		eprintf("expected %s but found ", expected);
		show_token(here);
		eprintf("\n");
	}
}

/***********************************************************************
 *      _            _ _  __ _
 *   __(_)_ __  _ __| (_)/ _(_)___ _ _   pratt parser
 *  (_-< | '  \| '_ \ | |  _| / -_) '_|  expression
 *  /__/_|_|_|_| .__/_|_|_| |_\___|_|    evaluator
 *             |_|
 */

/*
 * WG14 6.10.1p4	Conditional inclusion
 * WG21 [cpp.cond]p12	Conditional inclusion
 *
 * The controlling expression of a #if is either intmax_t or uintmax_t.
 *
 * We use a two's complement representation in uintmax_t to avoid some
 * questions about how C handles signed intmax_t. But we assume that
 * conversion from uintmax_t to intmax_t does the expected thing.
 *
 * When a nud or led function evaluates a subexpression, it returns a span
 * that covers its operand(s), operator, and internal whitespace, but not
 * surrounding whitespace.
 *
 * Important rules for the evaluator as a whole, and NUD and LED functions
 * in particular:
 *
 * At the start of a nud or led function, the lookahead token is the thing
 * being evaluated, which for led functions is the operator, and for nud
 * functions is a prefix operator or an operand. The nud or led function
 * advances the lexer past any tokens that it consumes. The eval() function
 * does not advance the lexer itself, only by calling nud and led
 * functions.
 *
 * After returning from a (possibly recursive) subexpression evaluation,
 * the lookahead token is the next thing to be looked at, e.g. an operator
 * or delimiter, not part of the subexpression. The caller must only
 * advance the lexer at this point if the caller needs to examine the
 * lookahead token after the subexpression, such as : in ?: or a closing ).
 */

typedef struct Integer {
	Inttype type;
	uintmax_t val;
	span s;
} Integer;

static void
integer_debug(Integer i) {
	switch(i.type) {
	case(SyntaxError):
		eprintf("Syntax error <- %.*s", spanf(i.s));
		return;
	case(ValueError):
		eprintf("Value error <- %.*s", spanf(i.s));
		return;
	case(Unknown):
		eprintf("Unknown <- %.*s", spanf(i.s));
		return;
	case(Boolean):
		eprintf("%s <- %.*s",
			i.val ? "true" : "false", spanf(i.s));
		return;
	case(Signed):
		eprintf("%+jd <- %.*s", (intmax_t)i.val, spanf(i.s));
		return;
	case(Unsigned):
		eprintf("%ju <- %.*s", i.val, spanf(i.s));
		return;
	}
}

static bool
valueless(Inttype type) {
	return(type == SyntaxError || type == ValueError || type == Unknown);
}

static Integer
interr(Inttype type, span s) {
	assert(valueless(type));
	return(Integer){ .type = type, .s = s, .val = 0 };
}

/*
 * During evaluation, we keep the current lookahead token (so we don't
 * have to return it alongside the value of a subexpression) and we
 * also need the lexer position and the list of saved deletions.
 */

typedef struct Expression {
	Position *here;
	Deleted *del;
} Expression;

/*
 * The usual way to finish evaluating a constant involves consuming the
 * constant's token from the lexer.
 */
static Integer
nud_value(Expression *e, Inttype type, uintmax_t val) {
	span s = e->here->txt;
	next(e->here);
	return(Integer){ .type = type, .s = s, .val = val };
}

/*
 * The simplifier has its own version of integer promotion, when a
 * boolean is promoted to an integer.
 *
 * When we have a boolean subexpression that's up for lazy simplification,
 * and its value is demanded by a non-boolean operator, we will need to
 * substitute a 0 or 1. Stash the subexpression on the deletion list to be
 * dealt with later.
 */
static Integer
promote(Expression *e, Integer i) {
	if(i.type == Boolean) {
		e->del = deleted(e->del, i.s,
				 i.val ? NothingBut1 : NothingBut0);
		i.type = Signed;
	}
	return(i);
}

/*
 * WG14 6.3.1.8           Usual arithmetic conversions
 * WG21 [expr.arith.conv] Usual arithmetic conversions
 *
 * In the preprocessor the only types are intmax_t and uintmax_t, so
 * the usual arithmetic conversions are reduced to saying the
 * operation is unsigned if either operand is unsigned. This function
 * is called after the operands have been promoted from boolean, but
 * even so, boolean operands act like signed.
 *
 * This function is also frequently used to handle error propagation.
 * The error cases are handled in order from most severe to less severe.
 */

#define PROPAGATE(TYPE)				    \
	if(left.type == TYPE || right.type == TYPE) \
		return(TYPE)

static Inttype
usual_arithmetic_conversions(Integer left, Integer right) {
	PROPAGATE(SyntaxError);
	PROPAGATE(ValueError);
	PROPAGATE(Unknown);
	PROPAGATE(Unsigned);
	return(Signed);
}

#undef PROPAGATE

// character constants -------------------------------------------------

/*
 * WG14 6.4.4.4		Character constants
 * WG14 6.10.1p4	Conditional inclusion
 * WG21 [lex.ccon]	Character literals
 * WG21 [cpp.cond]p12	Conditional inclusion
 *
 * We don't have to match the value the platform would give to a
 * character; nevertheless we try to match common practise for single
 * characters that are within the rules.
 *
 * Most of the validity checking has been done by the lexer.
 */
static const byte *
char_preamble(Expression *e) {
	const byte *p = backslash_newline(e->here->txt.ptr);
	while(*p != '\0' && strchr("LUu8", *p))
		p = backslash_newline(p+1);
	assert(*p == '\'');
	return(backslash_newline(p+1));
}

static const byte *
char_preamblesque(Expression *e) {
	const byte *p = char_preamble(e);
	assert(*p == '\\');
	return(backslash_newline(p+1));
}

// we could use some closures here
#define CHAR_MULTIBYTE(NAME,MASK,CONT)				\
	static uintmax_t NAME(Expression *e) {			\
		const byte *p = char_preamble(e);		\
		uintmax_t c = *p & MASK; CONT;			\
		return(c);					\
	} struct _ // allow trailing semicolon

#define UTF8CONT (p = backslash_newline(p + 1), \
		  c = (c << 6) | (*p & 0x3F));

CHAR_MULTIBYTE(char_ascii, 0x7F, (void)0);
CHAR_MULTIBYTE(char_byte, 0xFF, (void)0);
CHAR_MULTIBYTE(char_utf2, 0x1F, UTF8CONT);
CHAR_MULTIBYTE(char_utf3, 0x0F, UTF8CONT; UTF8CONT);
CHAR_MULTIBYTE(char_utf4, 0x07, UTF8CONT; UTF8CONT; UTF8CONT);

#undef CHAR_MULTIBYTE
#undef UTF8CONT

static uintmax_t
char_esc(Expression *e) {
	switch(*char_preamblesque(e)) {
	case('a'):	return('\a');
	case('b'):	return('\b');
	case('e'):	return(27); // GNU C extension
	case('f'):	return('\f');
	case('n'):	return('\n');
	case('r'):	return('\r');
	case('t'):	return('\t');
	case('v'):	return('\v');
	case('?'):	return('\?');
	case('\''):	return('\'');
	case('\"'):	return('\"');
	case('\\'):	return('\\');
	default: NEVER("expected a simple character escape sequence");
	}
}

static uintmax_t
char_oct(Expression *e) {
	const byte *p = char_preamblesque(e);
	uintmax_t c = 0;
	for(;; p = backslash_newline(p + 1)) {
		if ('0' <= *p && *p <= '7') c = (c << 3) | (*p - '0'); else
		return(c);
	}
}

static uintmax_t
char_hex(Expression *e) {
	const byte *p = char_preamblesque(e);
	uintmax_t c = 0;
	for(p = backslash_newline(p + 1); true; p = backslash_newline(p + 1)) {
		if ('0' <= *p && *p <= '9') c = (c << 4) | (*p - '0'); else
		if ('a' <= *p && *p <= 'f') c = (c << 4) | (*p - 'a' + 10); else
		if ('A' <= *p && *p <= 'F') c = (c << 4) | (*p - 'A' + 10); else
		return(c);
	}
}

static Integer
char_u(Expression *e, uintmax_t c) {
	return(nud_value(e, Unsigned, c));
}

static Integer
char_s(Expression *e, uintmax_t c) {
	return(nud_value(e, Signed, c));
}

/* sign-extend byte to integer */
static Integer
char_x(Expression *e, uintmax_t c) {
	uintmax_t neg = (c & 0x80) ? UINTMAX_MAX << CHAR_BIT : 0;
	return(nud_value(e, Signed, neg | c));
}

// character constant NUD functions maintained automatically
static NUD(CH_ascii_s) { return(char_s(e, char_ascii(e))); }
static NUD(CH_ascii_u) { return(char_u(e, char_ascii(e))); }
static NUD(CH_byte_u)  { return(char_u(e, char_byte(e))); }
static NUD(CH_byte_x)  { return(char_x(e, char_byte(e))); }
static NUD(CH_esc_s)   { return(char_s(e, char_esc(e))); }
static NUD(CH_esc_u)   { return(char_u(e, char_esc(e))); }
static NUD(CH_hex_s)   { return(char_s(e, char_hex(e))); }
static NUD(CH_hex_u)   { return(char_u(e, char_hex(e))); }
static NUD(CH_hex_x)   { return(char_x(e, char_hex(e))); }
static NUD(CH_oct_s)   { return(char_s(e, char_oct(e))); }
static NUD(CH_oct_u)   { return(char_u(e, char_oct(e))); }
static NUD(CH_oct_x)   { return(char_x(e, char_oct(e))); }
static NUD(CH_utf2_s)  { return(char_s(e, char_utf2(e))); }
static NUD(CH_utf2_u)  { return(char_u(e, char_utf2(e))); }
static NUD(CH_utf3_s)  { return(char_s(e, char_utf3(e))); }
static NUD(CH_utf3_u)  { return(char_u(e, char_utf3(e))); }
static NUD(CH_utf4_s)  { return(char_s(e, char_utf4(e))); }
static NUD(CH_utf4_u)  { return(char_u(e, char_utf4(e))); }
// end of maintained section

// integer constants ---------------------------------------------------

/*
 * WG14 6.4.4.1		Integer constants
 * WG14 7.8.2		Functions for greatest-width integer types
 * WG14 7.22.1		Numeric conversion functions
 * WG21 [lex.icon]	Integer literals
 *
 * This is easy but it doesn't handle backslash-newline and ' separators.
 */

static Integer
integer(Expression *e, int skip, int base, Inttype type) {
	const char *start = (SC)e->here->txt.ptr;
	char *end;
	errno = 0;
	uintmax_t val = strtoumax(start + skip, &end, base);
	end += strspn(end, "luLU");
	if(e->here->txt.end != (BC)end)
		type = SyntaxError;
	else if(val == UINTMAX_MAX && errno != 0)
		type = ValueError;
	else if(val > INTMAX_MAX)
		type = Unsigned;
	return(nud_value(e, type, val));
}

/*
 * The dispatch table refers to the same NUD functions
 * for LL and other integer constants.
 */

// integer constant NUD functions maintained automatically
static NUD(INTb2s)  { return(integer(e, 2,  2,   Signed)); }
static NUD(INTb2u)  { return(integer(e, 2,  2, Unsigned)); }
static NUD(INT_8s)  { return(integer(e, 0,  8,   Signed)); }
static NUD(INT_8u)  { return(integer(e, 0,  8, Unsigned)); }
static NUD(INT_10s) { return(integer(e, 0, 10,   Signed)); }
static NUD(INT_10u) { return(integer(e, 0, 10, Unsigned)); }
static NUD(INTx16s) { return(integer(e, 2, 16,   Signed)); }
static NUD(INTx16u) { return(integer(e, 2, 16, Unsigned)); }
// end of maintained section

/*
 * WG14 7.18		Boolean type and values <stdbool.h>
 * WG21 [lex.bool]	Boolean literals
 *
 * Boolean literals get promoted to intmax_t - see the discussion
 * about the usual_arithmetic_conversions() above.
 */

static NUD(FALSE) {
	return(nud_value(e, Signed, 0));
}

static NUD(TRUE) {
	return(nud_value(e, Signed, 1));
}

// tokens without values -----------------------------------------------

/*
 * We hit newline or end of file when expecting an expression. Unlike other
 * nud functions, we must not eat the lookahead token, but leave it in
 * place for use by the outer expression. Although next() called by
 * nud_value() will not move past a newline, we don't want this
 * subexpression's span to cover the newline so we can consistently treat
 * newlines as not being part of preprocessor directive or expressions.
 */
static NUD(nomore) {
	inexpressible(e->here, "unary expression or primary expression");
	return(interr(SyntaxError, span_left(e->here->txt, e->here->txt)));
}

/*
 * Something else unexpectedly horrible has turned up in our expression.
 * In this case we eat the error token like other nud functions.
 */
static NUD(error) {
	inexpressible(e->here, "unary expression or primary expression");
	return(nud_value(e, SyntaxError, 0));
}

/*
 * Character and integer constants that we don't know how to evaluate
 */
static NUD(novalue) {
	return(nud_value(e, ValueError, 0));
}

/*
 * Unknown symbols in an expression have an Unknown value.
 * We don't replace them with 0 because unlike the standard C
 * preprocessor we don't have complete knowledge of the program.
 */
static NUD(unknown) {
	return(nud_value(e, Unknown, 0));
}

/*
 * Similar considerations apply to any defined() operators that are left
 * around after macro expansion. If we see an expression like `defined
 * SYM` then we consume it. If the expression looks like `defined(SYM)`
 * then it will be handled by led_OPEN() below.
 */
static NUD(defined) {
	span s = e->here->txt;
	if(is_symbol(next(e->here))) {
		s = span2(s, e->here->txt);
		next(e->here);
	}
	return(interr(Unknown, s));
}

// round brackets ------------------------------------------------------

/*
 * We don't promote the inner subexpression so that if it can be
 * simplified then the bracketed expression can be too.
 */
static NUD(OPEN) {
	span open = e->here->txt;
	Integer i = eval_next(e, 0);
	if(e->here->tok == CLOSE) {
		i.s = span2(open, e->here->txt);
		next(e->here);
		return(i);
	} else {
		inexpressible(e->here, "')'");
		// span does not cover the lookahead token
		return(interr(SyntaxError, span2(open, i.s)));
	}
}

/*
 * A C preprocessor expression evaluator has a small selection of built-in
 * functions: the defined() operator and the newer __has_ tests. Normally,
 * all function-like macros should already have been expanded. But we don't
 * have full knowledge of the program so we can expect to see unexpanded
 * function-like macro invocations. So we parse function calls and treat
 * them as an unknown expression.
 *
 * This can cause unifdef to skim over things that a standard preprocessor
 * would treat as a syntax error, like a stray `sizeof()`.
 */
static LED(OPEN) {
	// Is this expression like `symbol(args)`?
	Integer i = left;
	if(i.type != Unknown) {
		i.type = SyntaxError;
	} else {
		Position back = *e->here;
		back.txt = (span){ i.s.ptr, i.s.ptr };
		if(!is_symbol(lex(&back)) || back.txt.end != i.s.end)
			i.type = SyntaxError;
	}
	///// call the macro argument code when it is written.
	Index nest = 0;
	for(Token tok = e->here->tok; inside_line(tok); tok = e->here->tok) {
		if(tok == OPEN) nest++;
		if(tok == CLOSE) nest--;
		i.s = span2(i.s, e->here->txt);
		next(e->here);
		if(nest == 0 && tok == CLOSE)
			break;
	}
	return(i);
}

// unary operators -----------------------------------------------------

/*
 * WG14 6.5.3.3		Unary arithmetic operators
 * WG21 [expr.unary.op]	Unary operators
 *
 * 28 is the right binding power of unary operators. It isn't in the
 * dispatch table because that only has left binding powers.
 */

// we could use some closures here
#define UNARY_NUD(NAME, OP, CODE)				\
	static NUD(NAME) {					\
		span op = e->here->txt;				\
		Integer i = eval_next(e, 28);			\
		CODE;						\
		i.val = OP i.val;				\
		i.s = span2(op, i.s);				\
		return(i);					\
	} struct _ // allow trailing semicolon

UNARY_NUD(COMPL, ~, {
		i = promote(e, i);
	});

UNARY_NUD(ADD, +, {
		i = promote(e, i);
	});

UNARY_NUD(SUB, -, {
		// detect overflow of most negative integer
		// (note i.val is uintmax_t)
		i = promote(e, i);
		if(i.type == Signed && i.val == 1+(uintmax_t)INTMAX_MAX)
			i.type = ValueError;
	});

UNARY_NUD(NOT, !, {
		// unary NOT gets simplified instead of promoting its argument
		if(i.type == Signed || i.type == Unsigned)
			i.type = Boolean;
	});

#undef UNARY_NUD

// binary operators ----------------------------------------------------

// binary operator implementations maintained automatically
static BINS(DIV)    { return(left /  right); }
static BINU(DIV)    { return(left /  right); }
static BINS(MOD)    { return(left %  right); }
static BINU(MOD)    { return(left %  right); }
static BINS(MUL)    { return(left *  right); }
static BINU(MUL)    { return(left *  right); }
static BINS(ADD)    { return(left +  right); }
static BINU(ADD)    { return(left +  right); }
static BINS(SUB)    { return(left -  right); }
static BINU(SUB)    { return(left -  right); }
static BINS(GE)     { return(left >= right); }
static BINU(GE)     { return(left >= right); }
static BINS(GT)     { return(left >  right); }
static BINU(GT)     { return(left >  right); }
static BINS(LE)     { return(left <= right); }
static BINU(LE)     { return(left <= right); }
static BINS(LT)     { return(left <  right); }
static BINU(LT)     { return(left <  right); }
static BINS(EQ)     { return(left == right); }
static BINU(EQ)     { return(left == right); }
static BINS(NOT_EQ) { return(left != right); }
static BINU(NOT_EQ) { return(left != right); }
static BINS(BITAND) { return(left &  right); }
static BINU(BITAND) { return(left &  right); }
static BINS(XOR)    { return(left ^  right); }
static BINU(XOR)    { return(left ^  right); }
static BINS(BITOR)  { return(left |  right); }
static BINU(BITOR)  { return(left |  right); }
// end of maintained section

// arithmetic exceptions -----------------------------------------------

/*
 * WG14 6.5.5		Multiplicative operators
 * WG21 [expr.mul]	Multiplicative operators
 *
 * Undefined behaviour from division or modulus? The standards could
 * be a bit more explicit about the hilarious MIN / -1 footgun...
 */
static Inttype
undivined(Integer left, Integer right) {
	Inttype t = usual_arithmetic_conversions(left, right);
	if(valueless(t)) return(t);
	if(right.val == 0 ||
	   (t == Signed &&
	    left.val == (uintmax_t)INTMAX_MIN &&
	    right.val == (uintmax_t)-1))
		return(ValueError);
	return(t);
}

/*
 * WG14 6.2.5p9		Types (unsigned integers)
 * WG14 6.5p5		Expressions (exceptional conditions)
 * WG21 [basic.fundamental] Fundamental type (unsigned integers)
 * WG21 [expr.pre]	Expressions - preamble (representable values)
 *
 * Unsigned arithmetic is modulo 2^N without overflow gotchas, whereas
 * signed overflow is undefined, so let's detect overflow in a few
 * operators.
 */

// we could use some closures here
#define UB40(COND) do {							\
		Inttype t = usual_arithmetic_conversions(left, right);	\
		if(t != Signed) return(t);				\
		intmax_t lv = (intmax_t)left.val;			\
		intmax_t rv = (intmax_t)right.val;			\
		if(COND) return(ValueError); else return(Signed);	\
	} while(0)

static Inttype
maltiply(Integer left, Integer right) {
	UB40((lv > 0 && rv > 0 && lv > INTMAX_MAX / rv) ||
	     (lv < 0 && rv > 0 && lv < INTMAX_MIN / rv) ||
	     (lv > 0 && rv < 0 && rv < INTMAX_MIN / lv) ||
	     (lv < 0 && rv < 0 && rv > INTMAX_MAX / lv));
}

static Inttype
attrition(Integer left, Integer right) {
	UB40((lv > 0 && rv > 0 && lv > INTMAX_MAX - rv) ||
	     (lv < 0 && rv < 0 && lv < INTMAX_MIN - rv));
}

static Inttype
submarine(Integer left, Integer right) {
	UB40((lv > 0 && rv < 0 && lv > INTMAX_MAX + rv) ||
	     (lv < 0 && rv > 0 && lv < INTMAX_MIN + rv));
}

#undef UB40

// shift operators -----------------------------------------------------

/*
 * WG14 6.5.7		Bitwise shift operators
 * WG21 [expr.shift]	Shift operators
 *
 * Shift operators in C consist almost entirely of undefined behaviour.
 * (But C++20 improves things - see below.)
 *
 * Can't shift by a negative amount, or by more than the width of a
 * word. We can do both checks in one because negatve numbers are very
 * large after conversion to unsigned.
 *
 * The result type matches the left operand.
 */
static Inttype
too_shifty(Integer left, Integer right) {
	Inttype t = usual_arithmetic_conversions(left, right);
	if(valueless(t)) return(t);
	if(right.val >= CHAR_BIT * sizeof(uintmax_t))
		return(ValueError);
	return(left.type);
}

/*
 * WG14 6.5.7		Bitwise shift operators
 * N4849 [expr.shift]	Shift operators
 *
 * We avoid some other horribleness by doing what C++20 says, which is
 * somewhat more specific about what implementations can do. C++20
 * requires arithmetic right shift, but in earlier standards signed
 * right shift is implementation defined. And C++20 says shifting a
 * negative value left is the same as casting it to unsigned then
 * shifting, whereas in earlier standards it is undefined behaviour.
 */

static BINS(SHL) {
	return((intmax_t)((uintmax_t)left << right));
}

static BINS(SHR) {
	if(left < 0) return(~(~left >> right));
	else return(left >> right);
}

static BINU(SHL) { return(left << right); }
static BINU(SHR) { return(left >> right); }

// binary operators ----------------------------------------------------

/*
 * WG14 6.5.8		Relational operators
 * WG14 6.5.9		Equality operators
 * WG21 [expr.rel]	Relational operators
 * WG21 [expr.eq]	Equality operators
 *
 * The result of a comparison is an int in C or a bool in C++, which
 * promotes to signed intmax_t in the preprocessor. We use Boolean as a
 * brief lie (these operators do not in fact get simplified) that tells
 * led_bin() to call the unsigned operator and cast the result to
 * signed.
 *
 * Nothing to do with undefined behaviour here, so no silly name.
 */
static Inttype
tub_cmp(Integer left, Integer right) {
	Inttype t = usual_arithmetic_conversions(left, right);
	return(t == Unsigned ? Boolean : t);
}

/*
 * Dispatch the non-shortcutting binary operators.
 *
 * Promote the operands so that lazily-simplified boolean
 * subexpressions will get their values substituted. Do type
 * propagation and preflight checks for undefined behaviour, then
 * dispatch to the right operator implementation.
 *
 * The split between tub and bin functions saves us from having to
 * repeat the signed/unsigned dispatch logic in each tub function.
 */
static LED(bin) {
	Token op = e->here->tok;
	Integer right = eval_next(e, token[op].lbp);
	Integer i = { .s = span2(left.s, right.s) };
	left = promote(e, left);
	right = promote(e, right);
	i.type = token[op].tub(left, right);
	if(i.type == Signed) {
		i.val = (uintmax_t)token[op].bins(
			(intmax_t)left.val, (intmax_t)right.val);
	} else if(i.type == Unsigned) {
		i.val = token[op].binu(left.val, right.val);
	} else if(i.type == Boolean) {
		// tub_cmp() lied but we know what it really meant
		i.type = Signed;
		i.val = token[op].binu(left.val, right.val);
	}
	return(i);
}

// shortcutting booleans -----------------------------------------------

/*
 * Declarations and other preparation for the macros below.
 *
 * This supports both left associativity (0 for && and ||) and right
 * associativity (1 for binary ?:).
 *
 * We use the usual arithmetic conversions for error propagation but ignore
 * any conversion. The error propagation is a bit different for these
 * operators in order to implement shortcutting more faithfully: if the
 * left operand is a value error, we can't prune it based on the value of
 * the right operand, but we can prune a right operand that is a value
 * error.
 */
#define PREAMBLE(op, assoc)						\
	Integer right = eval_next(e, token[op].lbp - assoc);		\
	span s = span2(left.s, right.s);				\
	Inttype type = usual_arithmetic_conversions(left, right);	\
	if(type == SyntaxError) return(interr(SyntaxError, s));		\
	if(left.type == ValueError) return(interr(ValueError, s));	\
	struct _ // allow trailing semicolon

/*
 * When the result is determined by either or both of the operands,
 * return a boolean to be simplified by some surrounding context.
 *
 * JOIN: conjunction / disjunction
 * BANG: false! or true!!
 */
#define BOOLE(BANG, var) (!valueless(var.type) && BANG var.val)

#define DE_MORGAN(JOIN, BANG)						\
	if(BOOLE(BANG, left) JOIN BOOLE(BANG, right))			\
	    return(Integer){ .s = s, .type = Boolean, .val = BANG true }; \
	else do {} while(0) // eat trailing semicolon

/*
 * When one operand is known to have no effect on the result and the other
 * is unknown, immediately simplify by completely deleting the known
 * operand and the operator, returning only the unknown operand.
 */
#define DROP_OTHER_KEEP(var) do {					\
		e->del = deleted(e->del, span_trim(s, var.s), NothingAtAll); \
		return(var);						\
	} while(0)

static LED(AND) {
	PREAMBLE(AND, 0);
	DE_MORGAN(||, !); // either false
	DE_MORGAN(&&, !!); // both true
	// chomp true
	if(BOOLE(!!, left))  DROP_OTHER_KEEP(right);
	if(BOOLE(!!, right)) DROP_OTHER_KEEP(left);
	return(interr(type, s));
}

static LED(OR) {
	PREAMBLE(OR, 0);
	DE_MORGAN(||, !!); // either true
	DE_MORGAN(&&, !); // both false
	// chomp false
	if(BOOLE(!, left))  DROP_OTHER_KEEP(right);
	if(BOOLE(!, right)) DROP_OTHER_KEEP(left);
	return(interr(type, s));
}

/*
 * https://gcc.gnu.org/onlinedocs/gcc/Conditionals.html
 *
 * Not boolean, really, but it is shortcutting.
 */
static Integer
binary_query_colon(Expression *e, Integer left) {
	PREAMBLE(QUERY, 1);
	if(BOOLE(!!, left)) DROP_OTHER_KEEP(left);
	if(BOOLE(!, left)) DROP_OTHER_KEEP(right);
	return(interr(type, s));
}

#undef PREAMBLE
#undef BOOLE
#undef DE_MORGAN
#undef DROP_OTHER_KEEP

// ternary operator ----------------------------------------------------

/*
 * WG14 6.5.15		Conditional operator
 * WG21 [expr.cond]	Conditional operator
 *
 * Unlike the other operators implemented by unifdef, this one is
 * right associative, so we adjust the binding power when calling
 * eval() for the right / falsy operand.
 */
static LED(QUERY) {
	if(next(e->here) == COLON)
		return(binary_query_colon(e, left));
	Integer truthy = eval(e, 0); // already called next()
	if(e->here->tok != COLON) {
		inexpressible(e->here, "':'");
		return(interr(SyntaxError, span2(left.s, truthy.s)));
	}
	Integer falsy = eval_next(e, token[QUERY].lbp-1);
	span s = span2(left.s, falsy.s);
	// similar error propagation considerations as && and ||
	if(left.type == SyntaxError ||
	   truthy.type == SyntaxError ||
	   falsy.type == SyntaxError)
		return(interr(SyntaxError, s));
	if(valueless(left.type))
		return(interr(left.type, s));
	// controlling expression is false, so delete everything
	// but the "else" branch like the boolean operators above
	if(left.val == 0) {
		e->del = deleted(e->del, span_left(s, falsy.s), NothingAtAll);
		return(falsy);
	}
	// delete everything but the "then" branch in two parts
	e->del = deleted(e->del, span_left(left.s, truthy.s), NothingAtAll);
	e->del = deleted(e->del, span_right(truthy.s, falsy.s), NothingAtAll);
	return(truthy);
}

// evaluate an expression ----------------------------------------------

/*
 * When we are evaluating an operator, the lookahead token is the operator
 * itself. It calls eval_next() to skip past the operator and the following
 * whitespace and evaluate its right operand.
 *
 * In typical Pratt parsers this happens in the core function, but some
 * operators (well, ?:) need to peek at the next token themselves.
 */
static Integer
eval_next(Expression *e, int rbp) {
	next(e->here);
	return(eval(e, rbp));
}

/*
 * This is the core of the Pratt top-down operator precedence parser.
 *
 * It's a recursive-descent precedence-climbing parser, which uses
 * a loop to avoid left recursion.
 *
 * A couple of things are necessary to keep this neat: the dispatch table
 * is set up to do all the error handling, so we don't need to check here;
 * and the error handling in the nud, led, and next() functions co-operate
 * to ensure we don't overrun.
 */
static Integer
eval(Expression *e, int rbp) {
	Token tok = e->here->tok;
	Integer i = token[tok].nud(e);
	tok = eval_debug(e, i, rbp);
	while(token[tok].lbp > rbp) {
		i = token[tok].led(e, i);
		tok = eval_debug(e, i, rbp);
	}
	return(i);
}

static Token
eval_debug(Expression *e, Integer i, int rbp) {
	Token tok = e->here->tok;
	assert(rbp >= 0);
	if(diagnostic(e->here, "debug", 'X')) {
		integer_debug(i);
		eprintf(" -- ");
		show_token(e->here);
		eprintf(" ( %d > %d )\n", token[tok].lbp, rbp);
	}
	return(tok);
}

static Tristate
simplify(Output *out, Position *here) {
	(void)out;
	Expression e = { here, NULL };
	Integer i = eval_next(&e, 0);
	FREE(e.del);
	if(inside_line(here->tok)) {
		inexpressible(here, "newline");
		skip_to_newline(here);
		return(Maybe);
	}
	if(valueless(i.type))
		return(Maybe);
	return(i.val ? True : False);
}

/***********************************************************************
 *  _ __ _ _ ___ _ __ _ _ ___  ___ ___ ____ ___
 * | '_ \ '_/ -_) '_ \ '_/ _ \/ __/ -_)_ -<_ -<  actually do
 * | .__/_| \___| .__/_| \___/\___\___/___/___/  the thing
 * |_|          |_|
 */

/*
 * Our overall result is the exit status and the output buffer. To work out
 * what they are we also pass need the symbol table and the write flags.
 *
 * The nested if-section state machines are kept on an explicit stack,
 * instead of using recursion.
 *
 * When we are working with a #if or #elif line we need to hold on to
 * the parts of the line briefly while we work out how much of it will
 * be propagated to the output: the hash mark and any leading
 * whitespace or comments; the keyword; and the expanded/simplified
 * expression.
 *
 * When the filename in a #line directive is the result of macro expansion,
 * we need to keep its backing buffer around.
 */
typedef struct Output {
	Status x;
	Buffer *buf;
	Symtab st;
	Set *wflag;
	IfStack *stack;
	span hash;
	span kw;
	Buffer *exp;
	Buffer *name;
} Output;

/*
 * This is mainly used for deleting # directives, but we also use it
 * for sanity checking and cleaning up after copying a directive to
 * the output.
 */
static IfState
drop_hash(Output *out, Position *here, IfState state) {
	assert(!inside_line(here->tok));
	out->hash = (span){0};
	out->kw = (span){0};
	FREE(out->exp);
	return(state);
}

static IfState
keep_hash(Output *out, Position *here, IfState state) {
	out->buf = buf_cat(out->buf, span2(out->hash, here->txt));
	return(drop_hash(out, here, state));
}

static IfState
keep_expr(Output *out, Position *here, IfState state) {
	///// temporary
	skip_to_newline(here);
	return(keep_hash(out, here, state));
	///// actually
	out->buf = buf_cat(out->buf, out->hash);
	out->buf = buf_cat(out->buf, out->kw);
	out->buf = buf_cat(out->buf, span_buf(out->exp));
	out->buf = buf_cat(out->buf, here->txt); // newline
	return(drop_hash(out, here, state));
}

static Symbol *
sym_here(Output *out, Position *here) {
	return(sym_find(&out->st, here->txt));
}

static bool
dropping(IfState state) {
	return(DropMin <= state && state <= DropMax);
}

static bool
keeping(IfState state) {
	return(KeepMin <= state && state <= KeepMax);
}

// ---------------------------------------------------------------------

/*
 * Preprocess a source file
 */
static void
preprocess(Output *out, Position *here) {
	IfState state = KeepOutside;

	// initial backslash-newline sequence
	out->buf = buf_cat(out->buf, here->txt);

	while(more_lines(here)) {
		span bol = here->txt;
		Token tok = first(here);
		if(tok == HASH) {
			// preprocessor directive
			tok = next(here);
			out->hash = span_left(bol, here->txt);
			out->kw = here->txt;
			state = token[tok].hash[state](out, here, state);
			continue;
		} else if(dropping(state)) {
			skip_to_newline(here);
			continue;
		} else if(keeping(state)) {
			///// macro expansion happens here
			while(inside_line(tok))
				tok = lex(here);
			// including the newline
			out->buf = buf_cat(out->buf, span2(bol, here->txt));
			continue;
		} else {
			NEVER("invalid IfState");
		}
	}
}

// ---------------------------------------------------------------------

/*
 * The standards describe a #if...#endif sequence as an "if-section",
 * and each directive and the source lines under its control comprise an
 * #if-group, #else-group, etc. (The "group" itself is just the source
 * following the directive, but we might not be strictly pedantic about
 * the way we use the terminology.)
 *
 * We talk about each group being "false", "true", or "maybe",
 * depending on whether the controlling expression simplified to
 * nothing-but-0, nothing-but-1, or an unknown value.
 *
 * It's helpful to consider a simplified version of the unifdef logic that
 * just works on a sequence of #elif directives. In this case, there is a
 * prefix consisting of false groups (which are dropped) mixed with maybe
 * groups (which are kept). The prefix can be followed with a true group,
 * which is kept, after which everything else is dropped.
 *
 * The complications come from handling the start of the sequence, which
 * must be spelled differently, and might lead to all directives being
 * deleted; and because we prefer to write #else rather than #elif 1; and
 * because #elif and #else cannot follow #else.
 *
 * IfState values are sorted with all the keep states before the drop
 * states to make it easier to handle program text lines in groups.
 * The (partial) order of states in the state machine is quite
 * different, something like:

	Outside
	-> FalseHead | TrueHead | MaybeBody

	FalseHead	// false #if followed by false #elifs
	-> TrueHead | ElseHead | MaybeBody

	TrueHead	// first non-false #if/#elif is true
	-> ElifNoEnd | ElseNoEnd

	ElifNoEnd,	// #elif/#else disappear completely
	-> ElseNoEnd

	// at this point we need to output a #if

	MaybeBody	// first non-false #(el)if is maybe
	-> MaybeBody | FalseBody | TrueBody | ElseBody

	FalseBody	// a false #elif after a maybe state
	-> MaybeBody | FalseBody | TrueBody | ElseBody

        TrueBody	// a true #elif after a maybe state
	-> ElifTail | ElseTail

	ElifTail	// drop #elif/#else but keep #endif
	-> ElseTail

 * We handle an out-of-sequence directive after #else by keeping it if and
 * only if we would have kept an #endif in the same place.
 *
 * Starting an if-section doesn't depend on the surrounding state, other
 * than whether we are keeping or dropping. When we're dropping we don't
 * even care about #if vs #ifdef.
 *
 * For other if-section directives, the function names suggest what states
 * we might move to. The #elif and #else tables are similar apart from the
 * marked entries which deal with the body / tail transition. We usually
 * keep #endif except for the no_end cases.
 */

// if(def) -------------------------------------------------------------

/*
 * Standard hash_fn implementation preamble.
 * Many of them do not need to examine the current state.
 */
#define HASH(NAME) \
	static IfState NAME(Output *out, Position *here, IfState unused state)

#define drop_IF drop_IFDEF

#define DIRECTIF(NAME)						\
	hash_##NAME = {						\
		/* in state ...		do action ... */	\
		/* KeepOutside */	keep_##NAME,		\
		/* KeepTrueHead */	keep_##NAME,		\
		/* KeepTrueBody */	keep_##NAME,		\
		/* KeepMaybeBody */	keep_##NAME,		\
		/* KeepElseHead */	keep_##NAME,		\
		/* KeepElseBody */	keep_##NAME,		\
		/* DropFalseHead */	drop_##NAME,		\
		/* DropFalseBody */	drop_##NAME,		\
		/* DropElifTail */	drop_##NAME,		\
		/* DropElseTail */	drop_##NAME,		\
		/* DropElifNoEnd */	drop_##NAME,		\
		/* DropElseNoEnd */	drop_##NAME,		\
	}

static Directif DIRECTIF(IF);
static Directif DIRECTIF(IFDEF);

#undef drop_IF

HASH(drop_IFDEF) {
	out->stack = push_if(out->stack, state);
	skip_to_newline(here);
	return(drop_hash(out, here, DropElifNoEnd));
}

HASH(keep_IFDEF) {
	out->stack = push_if(out->stack, state);
	bool ifndef = here->tok == K_ifndef;
	// does the next token have the same spelling as a known symbol?
	Token tok = next(here);
	Symbol *sym = sym_here(out, here);
	if(!is_symbol(tok) || inside_line(next(here)))
		goto error;
	if(sym == NULL || sym->type == NoIncludes)
		goto maybe;
	if(ifndef == is_undef(sym->type))
		return(drop_hash(out, here, KeepTrueHead));
	else	return(drop_hash(out, here, DropFalseHead));
error:	parse_nothing_after(here, "#ifdef or #ifndef");
maybe:	return(keep_hash(out, here, KeepMaybeBody));
}

HASH(keep_IF) {
	out->stack = push_if(out->stack, state);
	switch(simplify(out, here)) {
	case(Maybe): return(keep_expr(out, here, KeepMaybeBody));
	case(False): return(drop_hash(out, here, DropFalseHead));
	case(True):  return(drop_hash(out, here, KeepTrueHead));
	}
	NEVER("unreachable");
}

// elif ----------------------------------------------------------------

static Directif hash_ELIF = {
	/* in state ...		do action ... */
	/* KeepOutside */	elif_keep_wrong,
	/* KeepTrueHead */	elif_no_end,
	/* KeepTrueBody */	elif_drop,
	/* KeepMaybeBody */	elif_body, //
	/* KeepElseHead */	elif_no_end_wrong,
	/* KeepElseBody */	elif_keep_wrong,
	/* DropFalseHead */	elif_head,
	/* DropFalseBody */	elif_body, //
	/* DropElifTail */	elif_drop,
	/* DropElseTail */	elif_keep_wrong,
	/* DropElifNoEnd */	elif_no_end,
	/* DropElseNoEnd */	elif_no_end_wrong,
};

HASH(elif_head) {
	switch(simplify(out, here)) {
	case(False): return(drop_hash(out, here, DropFalseHead));
	case(True):  return(drop_hash(out, here, KeepTrueHead));
	case(Maybe):
		// we have become the start of the if-section
		out->kw = span_str("if");
		return(keep_expr(out, here, KeepMaybeBody));
	}
	NEVER("unreachable");
}

HASH(elif_body) {
	switch(simplify(out, here)) {
	case(False): return(drop_hash(out, here, DropFalseBody));
	case(Maybe): return(keep_expr(out, here, KeepMaybeBody));
	case(True):
		// we have become the end of the if-section
		out->buf = buf_cat(out->buf, out->hash);
		out->buf = buf_cat(out->buf, span_str("else"));
		out->buf = buf_cat(out->buf, here->txt); // newline
		return(drop_hash(out, here, KeepMaybeBody));
	}
	NEVER("unreachable");
}

HASH(elif_drop) {
	skip_to_newline(here);
	return(drop_hash(out, here, DropElifTail));
}

HASH(elif_no_end) {
	skip_to_newline(here);
	return(drop_hash(out, here, DropElifNoEnd));
}

HASH(elif_keep_wrong) {
	parse_unexpected(here, "#elif");
	return(keep_expr(out, here, state));
}

HASH(elif_no_end_wrong) {
	parse_unexpected(here, "#elif");
	return(drop_hash(out, here, DropElseNoEnd));
}

// else ----------------------------------------------------------------

static Directif hash_ELSE = {
	/* in state ...		do action ... */
	/* KeepOutside */	else_keep_wrong,
	/* KeepTrueHead */	else_no_end,
	/* KeepTrueBody */	else_drop,
	/* KeepMaybeBody */	else_keep, //
	/* KeepElseHead */	else_no_end_wrong,
	/* KeepElseBody */	else_keep_wrong,
	/* DropFalseHead */	else_head,
	/* DropFalseBody */	else_keep, //
	/* DropElifTail */	else_drop,
	/* DropElseTail */	else_keep_wrong,
	/* DropElifNoEnd */	else_no_end,
	/* DropElseNoEnd */	else_no_end_wrong,
};

static IfState
hash_else(Output *out, Position *here, hash_fn *hash, IfState state) {
	parse_nothing_after(here, "#else");
	return(hash(out, here, state));
}

HASH(else_head) {
	return(hash_else(out, here, drop_hash, KeepElseHead));
}

HASH(else_keep) {
	return(hash_else(out, here, keep_hash, KeepElseBody));
}

HASH(else_drop) {
	return(hash_else(out, here, drop_hash, DropElseTail));
}

HASH(else_no_end) {
	return(hash_else(out, here, drop_hash, DropElseNoEnd));
}

HASH(else_keep_wrong) {
	parse_unexpected(here, "#else");
	return(keep_hash(out, here, state));
}

HASH(else_no_end_wrong) {
	parse_unexpected(here, "#else");
	return(drop_hash(out, here, DropElseNoEnd));
}

// endif ---------------------------------------------------------------

static Directif hash_ENDIF = {
	/* in state ...		do action ... */
	/* KeepOutside */	endif_wrong,
	/* KeepTrueHead */	endif_no_end,
	/* KeepTrueBody */	endif_keep,
	/* KeepMaybeBody */	endif_keep,
	/* KeepElseHead */	endif_keep,
	/* KeepElseBody */	endif_keep,
	/* DropFalseHead */	endif_no_end,
	/* DropFalseBody */	endif_keep,
	/* DropElifTail */	endif_keep,
	/* DropElseTail */	endif_keep,
	/* DropElifNoEnd */	endif_no_end,
	/* DropElseNoEnd */	endif_no_end,
};

HASH(endif_keep) {
	parse_nothing_after(here, "#endif");
	return(keep_hash(out, here, pop_if(out->stack)));
}

HASH(endif_no_end) {
	parse_nothing_after(here, "#endif");
	return(drop_hash(out, here, pop_if(out->stack)));
}

HASH(endif_wrong) {
	parse_unexpected(here, "#endif");
	return(keep_hash(out, here, state));
}

// ---------------------------------------------------------------------

/*
 * The non-if-section directives.
 *
 * When dropping a group, the standards say "directives are processed
 * only through the name that determines the directive in order to keep
 * track of the level of nested conditionals". This implies we do not
 * need to worry about lexing #include super accurately. (In any case,
 * it is undefined behaviour to write a header name that would get the
 * lexer confused when it isn't lexed strictly as a header name.)
 */

// these all do the same thing
#define drop_def  drop_non
#define drop_inc  drop_non
#define drop_line drop_non

static Directif DIRECTIF(non);  // not a directive
static Directif DIRECTIF(def);  // #define etc.
static Directif DIRECTIF(inc);  // #include etc.
static Directif DIRECTIF(line); // #line etc.

#undef drop_non
#undef drop_def
#undef drop_inc
#undef drop_line

// ---------------------------------------------------------------------

static IfState
drop_non(Output *out, Position *here, IfState state) {
	skip_to_newline(here);
	return(drop_hash(out, here, state));
}

static IfState
keep_non(Output *out, Position *here, IfState state) {
	skip_to_newline(here);
	return(keep_hash(out, here, state));
}

/*
 * Maybe delete a #define or #undef
 */
static IfState
keep_def(Output *out, Position *here, IfState state) {
	// does the next token have the same spelling as a known symbol?
	next(here);
	Symbol *sym = sym_here(out, here);
	skip_to_newline(here);
	if(sym != NULL && sym->type == NoDefines)
		return(drop_hash(out, here, state));
	else	return(keep_hash(out, here, state));
}

/*
 * WG14 6.4.7		Header names
 * WG14 6.10.2		Source file inclusion
 * WG21 [lex.header]	Header names
 * WG21 [cpp.include]	Source file inclusion
 *
 * Maybe delete a #include
 *
 * See the specialist lexer in header_name() above, and the note at the
 * start of this section explaining that we don't always have to lex
 * header names accurately.
 */
static IfState
keep_inc(Output *out, Position *here, IfState state) {
	Token tok = header_name(here);
	if(tok != HEADER)
		{} ///// macro expansion happens here
	Symbol *sym = sym_here(out, here);
	skip_to_newline(here);
	if(sym != NULL && sym->type == NoIncludes)
		return(drop_hash(out, here, state));
	else	return(keep_hash(out, here, state));
}

/*
 * WG14 6.10.4		Line control
 * WG21 [cpp.line]	Line control
 *
 * Update line number and maybe filename.
 *
 * I don't think I can be polite about this part of the standards. I mean,
 * #include is gratuitously weird in an obvious way, but #line is sly and
 * seems perfectly normal on the surface, but on closer examination it is
 * like some kind of psychopath home invasion that cannot understand why
 * it should follow the normal rules of the rest of the language.
 *
 * #line has its own completely unique syntax for numbers and strings,
 * which prevents us from reusing a lot of lexer code that would otherwise
 * be helpful. We can't re-use integer and string tokens, but have to have
 * extra tokens just for #line, and we can't re-use our integer evaluator
 * because #line allows decimal numbers with leading zeroes. I can't even.
 */
static IfState
keep_line(Output *out, Position *here, IfState state) {
	uintmax_t line = 0;
	Token tok = here->tok;
	if(tok == K_line) {
		tok = next(here);
		///// macro expansion happens here
		///// stash in out->name
	}
	if(tok != DIGITS && tok != ODIGITS)
		goto done;
	const char *start = (SC)here->txt.ptr;
	char *end;
	line = strtoumax(start, &end, 10);
	if(here->txt.end != (BC)end || line > INT32_MAX)
		line = 0;
	tok = next(here);
	if(tok != SSTRING) {
		if(inside_line(tok) &&
		   diagnostic(here, "error", 'e')) {
			eprintf("malformed #line file name\n");
			out->x |= Problem;
		}
		goto done;
	}
	here->name = (span){
		(BC)strchr((SC)here->txt.ptr, '"') + 1,
		(BC)strrchr((SC)here->txt.ptr, '"') };
done:	if(line > 0) {
		here->linext = line;
	} else if(diagnostic(here, "error", 'e')) {
		eprintf("malformed #line number\n");
		out->x |= Problem;
	}
	///// I am too cross to fill this in
	skip_to_newline(here);
	return(keep_hash(out, here, state));
}

/***********************************************************************
 *  _                _            __ _ _
 * | |_  ___ __ _ __| |___ _ _   / _(_) |___   #define
 * | ' \/ -_) _` / _` / -_) '_| |  _| | / -_)  #defval
 * |_||_\___\__,_\__,_\___|_|   |_| |_|_\___|  #undef
 */

/*
 * WG14 6.10.3		Macro replacement
 * WG14 6.10.6		Pragma directive
 * WG21 [cpp.replace]	Macro replacement
 * WG21 [cpp.pragma]	Pragma directive
 *
 * As well as the standard directives for defining and undefining
 * macros, unifdef has a few directives of its own. They can be used
 * as they are, or more portably as #pragma directives.
 */

/*
 * We are violating a constraint here by allowing redefinitions.
 */
static void
define(Symtab *st, Position *here, span name, Symbol sym) {
	Hash h = tokhash(name);
	Index i = tab_hfind(st->t, h, name);
	if(i == MISSING) {
		st->t = tab_hadd(st->t, h, name);
		st->s = sym_add(st->s, sym);
		return;
	}
	Symbol *found = &st->s->sym[i];
	if(!is_undef(sym.type) &&
	   !is_undef(found->type) &&
	   diagnostic(here, "warning", 'r'))
		eprintf("%.*s is being redefined\n", spanf(name));
	FREE(found->param);
	*found = sym;
}

static void
undefine(Symtab *st, Position *here, span name, Symtype type) {
	define(st, here, name, (Symbol){ type, NULL, (span){0} });
	skip_to_newline(here);
}

// apologies to all rock leopards out there
#define DEF_ERRARD(message) do {				\
		parse_expected(here, message, "skipping line");	\
		goto error;					\
	} while(0)

/*
 * WG14 6.10.3.2	The # operator
 * WG14 6.10.3.3	The ## operator
 * WG21 [cpp.stringize]	The # operator
 * WG21 [cpp.concat]	The ## operator
 *
 * The # operator must be followed by a parameter in function-like macros.
 * The ## operator is not allowed at the start or end of a macro body.
 */
static span
macro_body(Position *here, Table *param) {
	Token tok = next(here);
	if(here->tok == PASTE)
		DEF_ERRARD("start of macro body");
	// empty macro
	if(!inside_line(here->tok))
		return(span_left(here->txt, here->txt));
	// stash first token
	span body = here->txt;
	bool paste = false;
	for(; inside_line(tok); tok = lex(here)) {
		// not added to the body unless we see some other token
		if(spacial(tok))
			continue;
		if(tok == HASH && param != NULL) {
			tok = next(here);
			if(!is_symbol(tok) ||
			   tab_find(param, here->txt) == MISSING)
				DEF_ERRARD("macro parameter");
		}
		// so we can check this paste isn't the last token
		if(tok == PASTE) paste = true;
		else paste = false;
		body = span2(body, here->txt);
	}
	if(paste)
		DEF_ERRARD("end of macro body");
	return(body);
error:
	skip_to_newline(here);
	return((span){0});
}

/*
 * We treat standard __VA_ARGS__ like a special case of GNU C args... by
 * using __vA_ARGS__ as the name of the rest parameter if it doesn't
 * have a GNU C name. During expansion the two are handled the same way.
 */
static Status
function_like(Symtab *st, Position *here, span name) {
	span rest = span_str("__VA_ARGS__");
	Symtype type = FunctionLike;
	Table *param = tab_new();
	span body = {0};

	Token tok = next(here);
	if(tok == CLOSE)
		goto close;
	for(;;) {
		if(tok == REST) {
			param = tab_add(param, rest);
			type = VariadicFun;
			tok = next(here);
		} else if(is_symbol(tok)) {
			if(tab_find(param, here->txt) != MISSING)
				DEF_ERRARD("unique parameter");
			param = tab_add(param, here->txt);
			tok = next(here);
			// GNU C extension: named variadic arguments like
			// MACRO(one, two, rest...)
			if(tok == REST) {
				type = VariadicFun;
				tok = next(here);
			}
		} else {
			DEF_ERRARD("symbol or '...'");
		}
		if(tok == CLOSE)
			goto close;
		if(type == VariadicFun)
			DEF_ERRARD("')' after '...'");
		if(tok != COMMA)
			DEF_ERRARD("comma");
		tok = next(here);
	}
close:
	body = macro_body(here, param);
	if(span_is_null(body))
		goto error;
	define(st, here, name, (Symbol){ type, param, body });
	return(OK);
error:
	FREE(param);
	skip_to_newline(here);
	return(Problem);
}

/*
 * Ingest a -f header file.
 *
 * To allow an unifdef -f header to be used as a standard header, our
 * nonstandard preprocessor directives (in fact, for simplicity, all
 * directives) can be prefixed with `pragma unifdef`.
 */
static Status
header_parse(Symtab *st, Position *here) {
	Status x = OK;
	while(more_lines(here)) {
		Token tok = first(here);
		if(!inside_line(tok))
			continue;
		if(tok != HASH)
			DEF_ERRARD("#");

		tok = next(here);
		if(tok == K_pragma) {
			tok = next(here);
			if(tok == K_unifdef) {
				tok = next(here);
			} else {
				skip_to_newline(here);
				continue;
			}
		}

		Symtype type;
		switch(tok) {
		case(K_define):     type = ObjectLike; break;
		case(K_defval):     type = ValueOnly;  break;
		case(K_undef):      type = Undefined;  break;
		case(K_nodefines):  type = NoDefines;  break;
		case(K_noincludes): type = NoIncludes; break;
		default: DEF_ERRARD("#define or #undef, etc.");
		}

		if(type == NoIncludes) {
			tok = header_name(here);
			if(tok != HEADER)
				DEF_ERRARD("header name");
			undefine(st, here, here->txt, type);
			continue;
		}

		tok = next(here);
		if(!is_symbol(tok))
			DEF_ERRARD("symbol");
		span name = here->txt;

		if(is_undef(type)) {
			tok = next(here);
			if(inside_line(tok))
				DEF_ERRARD("nothing after a symbol");
			undefine(st, here, name, type);
			continue;
		}

		// do not skip whitespace after the macro name!
		tok = lex(here);
		if(tok == OPEN) {
			x |= function_like(st, here, name);
			continue;
		}
		if(!spacial(tok))
			DEF_ERRARD("whitespace after a symbol");

		Table *param = NULL;
		span body = macro_body(here, param);
		if(span_is_null(body)) x |= Problem;
		else define(st, here, name, (Symbol){ type, param, body });
		continue;
	error:
		skip_to_newline(here);
		x |= Problem;
		continue;
	}
	return(x);
}

static Status
header_slurp(Symtab *st, const char *name) {
	Buffer *buf = buf_slurp(name);
	Position there = lex_start(span_str(name), span_buf(buf));
	Position *here = &there;
	st->buflist = buflist_add(st->buflist, buf);
	return(header_parse(st, here));
}

// that's more than enough of that
#undef DEF_ERRARD

static void
Dflag(Symtab *st, const char *opt) {
	Position there = lex_start(span_str("-D"), span_str(opt));
	Position *here = &there;
	Token tok = lex(here);
	if(!is_symbol(tok)) goto usage;
	span name = here->txt;
	Table *param = NULL;
	span body = {0};
	tok = lex(here);
	if(tok == ASSIGN) {
		body = macro_body(here, param);
		if(span_is_null(body)) goto usage;
	} else if(tok == NOMORE) {
		body = span_str("1");
	} else {
		goto usage;
	}
	define(st, here, name, (Symbol){ ValueOnly, param, body });
	return;
usage:	fail("usage: -Dsym[=val] not %s", opt);
}

static void
Uflag(Symtab *st, const char *opt) {
	Position there = lex_start(span_str("-U"), span_str(opt));
	Position *here = &there;
	Token tok = lex(here);
	if(!is_symbol(tok)) goto usage;
	span name = here->txt;
	tok = lex(here);
	if(tok != NOMORE) goto usage;
        undefine(st, here, name, Undefined);
	return;
usage:	fail("usage: -Usym not %s", opt);
}

/***********************************************************************
 *                                   _   _ _
 *   __ ___ _ __  _ __  __ _ _ _  __| | | (_)_ _  ___   command
 *  / _/ _ \ '  \| '  \/ _` | ' \/ _` | | | | ' \/ -_)  line
 *  \__\___/_|_|_|_|_|_\__,_|_||_\__,_| |_|_|_||_\___|  options
 */

/*
 * The @(#) ... magic is used by SCCS `what` to find embedded revision
 * details. The $Keyword: ... $ syntax is similarly used by RCS `ident`.
 * Or you can run `unifdef -V`.
 */
static const char version[] =
/*!include:re2c "version.h" *//*!ignore:re2c
 */	"@(#) $Author: Tony Finch (dot@dotat.at) $\n"
	"@(#) $URL: https://dotat.at/prog/unifdef3/ $\n"
;

static noreturn void
print_version(void)
{
	bool print = false;
	for(const char *p = version; true; p++) {
		switch(*p) {
		case('\0'):	exit(0);
		case('\n'):	putchar(*p);		continue;
		case('$'):	print = ! print;	continue;
		default:	if(print) putchar(*p);	continue;
		}
	}
}

static void
synopsis(FILE *fp)
{
	fprintf(fp,
	"usage:	%s [-hV] [-dflags] [-wflags] [-xN] [-opath|-Mext|-m] \\\n"
	"		[-Dsym[=val]] [-Usym] [-fpath]... [source]...\n"
		, progname);
}

static noreturn void
help(void) {
	synopsis(stdout);
	printf(
	"	-Dsym=val	define symbol with given value\n"
	"	-Dsym		define symbol with value 1\n"
	"	-Usym		symbol is undefined\n"
	"	-f<path>	contains #define and #undef directives\n"
	"	-o<path>	output file name\n"
	"	-M<ext>		modify in place and keep backups\n"
	"	-m		modify input files in place\n"
	"	-d<flags>	diagnostic options\n"
	"	-w<flags>	output options\n"
	"	-x{012}		exit status mode\n"
	"	-h		print help\n"
	"	-V		print version\n"
	);
	exit(0);
}

static noreturn void
usage(void) {
	synopsis(stderr);
	exit(2);
}

static void
unifdef(Output *out, const char *ifn, const char *ext, const char *ofn) {
	Buffer *src = buf_slurp(ifn);
	Position there = lex_start(span_str(ifn), span_buf(src));
	Position *here = &there;

	preprocess(out, here);

	if(ext != NULL && ext[0] != '\0') {
		Buffer *save = buf_cat(NULL, span_str(ifn));
		save = buf_cat(save, span_str(ext));
		rename(ifn, (SC)save->base);
		FREE(save);
	}
	if(ext != NULL)
		buf_spew(out->buf, ifn);
	else
		buf_spew(out->buf, ofn);

	FREE(src);
	FREE(out->buf);
	FREE(out->stack);
	FREE(out->name);
}

extern int
main(int argc, char *argv[])
{
	progname = strrchr(argv[0], '/');
	if(progname) progname++;
	else progname = argv[0];

	// Ensure diagnostic messages are written a line at a time
	setvbuf(stderr, NULL, _IOLBF, 0);

	Output out = {0};

	const char *outfile = NULL;
	const char *modify = NULL;
	int exitmode = 0;

	static const char flags[] = "d:D:U:f:I:M:o:w:x:bBcehKklmnsStV";
	int opt;
	while((opt = getopt(argc, argv, flags)) != -1)
		switch(opt) {
		case('D'): // defval a symbol
			Dflag(&out.st, optarg);
			continue;
		case('U'): // undef a symbol
			Uflag(&out.st, optarg);
			continue;
		case('f'): // header file
			out.x |= header_slurp(&out.st, optarg);
			continue;
		case('m'): // modify in place
		case('M'): // and keep backup
			if(modify != NULL)
				fail("the -m/-M options can only be used once");
			modify = (opt == 'm') ? "" : optarg;
			continue;
		case('o'): // output to a file
			if(outfile != NULL)
				fail("the -o option can only be used once");
			outfile = optarg;
			continue;
		case('d'): // diagnostic options
			dflags = set_flags(dflags, (BC)optarg);
			continue;
		case('w'): // write options
			out.wflag = set_flags(out.wflag, (BC)optarg);
			continue;
		case('h'):
			help();
		case('V'):
			print_version();
		case('x'): // exit mode
			exitmode = atoi(optarg);
			if(exitmode < 0 || 2 < exitmode)
				fail("usage: -x {0|1|2}");
			continue;
		case('e'): // was fewer errors from dodgy lines
		case('I'): // for compatibility with cpp
			continue;
		case('b'): // blank deleted lines instead of omitting them
		case('B'): // compress blank lines around removed section
		case('k'): // process constant #ifs
		case('n'): // add #line directive after deleted lines
		case('s'): // list #if controlling symbols
			out.wflag = set_add(out.wflag, (Index)opt);
			goto todo;
		case('l'): // backwards compatibility
			out.wflag = set_add(out.wflag, 'b');
			continue;
		case('c'): // treat -D as -U and vice versa
		case('i'): // selectively disable lexing
		case('K'): // no && || shortcutting
		case('S'): // list symbols with nesting depth
		case('t'): // globally disable lexing
		todo:
			fail("the -%c option is no longer supported", opt);
		default:
			usage();
		}
	argc -= optind;
	argv += optind;

	if(out.x != OK)
		fail("erroneous -f header file");
	if(outfile != NULL && modify != NULL)
		fail("-o and -m/-M are mutually exclusive");
	if(argc > 1 && outfile != NULL)
		fail("-o cannot be used with multiple input files");
	if(argc > 1 && modify == NULL)
		fail("multiple input files require -m or -M");
	if(outfile == NULL)
		outfile = "-";

	symtab_debug(&out.st);

	if(argc == 0)
		unifdef(&out, "-", modify, outfile);
	else for(int i = 0; i < argc; i++)
		unifdef(&out, argv[i], modify, outfile);

	symtab_free(&out.st);

	if(out.x > 1)
		return(Problem);
	if(exitmode == 2)
		return(OK);

	return((int)out.x ^ exitmode);
}

/**********************************************************************/
