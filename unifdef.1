.\"             _  __    _      __
.\"   _  _ _ _ (_)/ _|__| |___ / _|  unifdef
.\"  | || | ' \| |  _/ _` / -_)  _|  expand macros
.\"   \_,_|_||_|_|_| \__,_\___|_|    reduce #ifdefs
.\"
.\" Written by Tony Finch <dot@dotat.at>
.\" SPDX-License-Identifier: 0BSD OR MIT-0
.\"
.Dd Feb 28, 2024
.Dt UNIFDEF 1
.Os UNIFDEF(1)
.Sh NAME
.Nm unifdef
.Nd expand macros, reduce #ifdefs
.Sh SYNOPSIS
.Nm
.Op Fl hV
.Op Fl d Ns Ar flags
.Op Fl w Ns Ar flags
.Op Fl x Bro Ar 012 Brc
.Op Fl m | Fl M Ns Ar ext | Fl o Pa outfile
.Op Fl D Ns Ar sym Ns Op = Ns Ar val
.Op Fl U Ns Ar sym
.Op Fl f Pa header
.Ar ...
.Op Pa source ...
.\"  ___  ___ ___  ___ ___ ___ ___ _____ ___ ___  _  _
.\" |   \| __/ __|/ __| _ \_ _| _ \_   _|_ _/ _ \| \| |
.\" | |) | _|\__ \ (__|   /| ||  _/ | |  | | (_) | .` |
.\" |___/|___|___/\___|_|_\___|_|   |_| |___\___/|_|\_|
.\"
.Sh DESCRIPTION
The
.Nm
utility is a partial C preprocessor.
It can expand just the macros you want expanded,
selectively process
.Li #ifdef
conditional directives,
and simplify
.Li #if
controlling expressions.
It does
.Em not
process
.Li #include
directives.
It preserves whitespace and comments.
.Pp
It processes one or more source files
according to a collection of macro names
which you can specify on the command line with the
.Fl D
or
.Fl U
options
or using preprocessor directives in
.Fl f Pa header
files.
.\" --------------------------------------------------------------------
.Sh OPTIONS
.Bl -tag -width indent
.It Fl D Ns Ar sym Ns = Ns Ar val
The symbol is defined to the given value value,
like
.Li #defval Ar sym Ar val
in a
.Fl f Pa header
file.
.Pp
.It Fl D Ns Ar sym
The symbol is defined to the value 1,
like
.Li #defval Ar sym 1
in a
.Fl f Pa header
file.
.Pp
.It Fl U Ns Ar sym
The symbol is explicitly undefined,
like
.Li #undef Ar sym
in a
.Fl f Pa header
file.
.Pp
.It Fl f Pa header
Read symbol definitions and undefinitions from the
.Pa header
file.
.Pp
The
.Fl D ,
.Fl U ,
and
.Fl f Pa header
options are processed in order,
and later definitions and undefinitions override earlier ones.
.Pp
.It Fl o Ar outfile
Write output to the file
.Ar outfile
instead of the standard output when processing a single file.
.Pp
.It Fl M Ns Ar ext
Modify multiple
.Pa source
files in place, and keep backups of the originals by
appending the
.Ar ext
to the
.Pa source
file names.
.Pp
.It Fl m
Modify multiple input files in place.
If an input file is not modified,
the original is preserved instead of being overwritten with an identical copy.
.Pp
.It Fl d Ns Ar flags
Set diagnostic options.
See the
.Sx DIAGNOSTICS
section below.
.Pp
.It Fl w Ns Ar flags
See the
.Sx Write options
subsection below.
.It Fl x Bro Ar 012 Brc
See the
.Sx EXIT STATUS
section below.
.It Fl h
Print help.
.It Fl V
Print version details.
.El
.\" --------------------------------------------------------------------
.Ss Write options
The
.Fl w Ns Ar flags
option allows you to control certain details of how
.Pa source
files are processed,
request optional features in the output,
or enable special output modes.
.\" --------------------------------------------------------------------
.Sh INPUT FILES
Source files can be specified on the command line after the options.
They are expected to be written in C or C++,
or some other language that has a sufficiently similar lexical syntax.
.Pp
The
.Fl f Pa header
files contain a limited set of preprocessor directives
that define or undefine symbols as described in the
.Sx EXTENDED DESCRIPTION
section below.
.\" --------------------------------------------------------------------
.Sh OUTPUT FILES
The output of
.Nm
is the partially preprocessed source file(s).
Unlike the standard preprocessor,
.Nm
preserves comments and whitespace.
.Pp
The
.Fl w Ar flags
control features of the output
such as
.Li #line
directives.
When there is one
.Pa source
file, you can use
.Fl o Ar output
to specify the output file name.
A
.Fl M Ns Ar ext
or
.Fl m
option tells
.Nm
to modify multiple
.Pa source
files in place, with or without backups.
.\" --------------------------------------------------------------------
.Sh STDIN
The source is read from the standard input
if no
.Pa source
file names are provided on the command line.
If a
.Fl f Pa header
file name or
.Pa source
file name is
.Ql -
the file is read from the standard input.
.\" --------------------------------------------------------------------
.Sh STDOUT
When there is one input file,
and no
.Fl M ,
.Fl m ,
or
.Fl o
option,
the partially preprocessed source is written to the standard output.
The
.Fl o Li -
option explicitly tells
.Nm
to write to the standard output.
.\" --------------------------------------------------------------------
.Sh STDERR
Errors and warnings are written to the standard error
as described in the
.Sx DIAGNOSTICS
section below.
The
.Fl d Ar flags
adjust diagnostic messages.
.\" --------------------------------------------------------------------
.Sh EXIT STATUS
If the exit mode is
.Fl x0
(the default),
.Nm
exits with status 0 if the output is an exact copy of the input,
or with status 1 if the output differs.
.Pp
If the exit mode is
.Fl x1 ,
then the status is inverted.
.Pp
If the exit mode is
.Fl x2 ,
then the status is 0 in either case.
.Pp
The
.Fl h
and
.Fl V
options exit with status 0.
.Pp
If there is an error,
.Nm
exits with a status 2.
.\"  ___  ___ _____ _   ___ _    ___
.\" |   \| __|_   _/_\ |_ _| |  / __|
.\" | |) | _|  | |/ _ \ | || |__\__ \
.\" |___/|___| |_/_/ \_\___|____|___/
.\"
.Sh EXTENDED DESCRIPTION
You use the
.Fl D
or
.Fl U
options
or preprocessor directives in
.Fl f Pa header
files
to specify a set of
.Dq symbols .
Symbols can be C preprocessor macros
or they can have a special
.Nm
meaning:
.Bl -tag -width indent
.It Em Function-like macros
created by
.Li #define Ar macro Ns Li \&( Ns Ar params Ns Li \&) Ar body
in
.Fl f Pa header
files
.It Em Object-like macros
created by
.Li #define Ar macro Ar value
in
.Fl f Pa header
files
.It Em Defined values
affect preprocessor conditions,
but are not expanded in the program text like macros;
they are set using the
.Fl D Ns Ar sym
or
.Fl D Ns Ar sym Ns Li = Ns Ar val
options or by
.Li #defval Ar sym Ar val
in
.Fl f Pa header
files
.It Em Explicitly undefined
using the
.Fl U Ns Ar sym
option or with
.Li #undef Ar sym
in
.Fl f Pa header
files
.It Em Unknown symbols
are other macros, identifiers, or keywords
that you did not specify a meaning for.
.El
.Pp
Each
.Fl f Pa header
file can only contain
.Li #defval ,
.Li #define ,
and/or
.Li #undef
directives,
with comments and extra white space.
No other preprocessor directives or program text is allowed.
.Pp
Given these symbols,
.Nm
partially pre-processes one or more source files
as described in the following sub-sections.
.\" --------------------------------------------------------------------
.Ss Macro expansion
Macros
(from
.Li #define )
are expanded in source files in the usual way,
in program text and in
.Li #if ,
.Li #elif ,
and
.Li #include
preprocessor directives.
Defined values
(from
.Li #defval )
are only subsituted into
.Li #if
and
.Li #elif
expressions, not the program text.
.Pp
Macros, defined values, and explicitly undefined symbols
can be tested by the
.Sy defined Ns Li \&( Ns Ar sym Ns Li \&)
operator and
.Li #ifdef ,
.Li #ifndef ,
.Li #elifdef ,
and
.Li #elifndef
conditions.
Unknown symbols produce an unknown result from the
.Sy defined
operator, instead of being treated as undefined.
.Pp
Any
.Li #defval ,
.Li #define ,
and
.Li #undef
directives are ignored in source files.
.Li #include
directives can be changed by macro expansion
but
.Nm
does not do any file inclusion.
.\" --------------------------------------------------------------------
.Ss Conditional reduction
The
.Nm
utility got its name from selectively deleting
.Li #ifdef ,
.Li #ifndef ,
.Li #if ,
.Li #elif ,
.Li #elifdef ,
.Li #elifndef ,
.Li #else ,
and
.Li #endif
lines,
and the sections of program text they control.
.Pp
.Li #ifdef Ar sym
is equivalent to
.Li #if Sy defined Ns Li \&( Ns Ar sym Ns Li \&) ,
and
.Li #ifndef Ar sym
is equivalent to
.Li #if \&! Ns Sy defined Ns Li \&( Ns Ar sym Ns Li \&) ,
but they are not simplified like
.Li #if
when they are retained.
.Pp
The controlling expression of a
.Li #if
is evaluated as described in the
.Sx Expression simplification
subsection below.
When the whole expression can be completely evaluated
and it simplifies to nothing,
the
.Li #if
is deleted.
Otherwise the simplified expression is
written to the output.
.Pp
When the
.Li #if
expression evaluates to
.Ar nothing-but-1
then just the directive is deleted
and the following lines under its control are retained.
The
.Li #if Ar nothing-but-1
group can be followed by
.Li #elif
or
.Li #else
directives which are deleted
along with all the program text under their control.
The matching
.Li #endif
is also deleted.
.Pp
When the
.Li #if
expression evaluates to
.Ar nothing-but-0
then the following lines under its control are deleted too.
When the
.Li #if Ar nothing-but-0
group is followed by
.Li #else
or
.Li #endif ,
then the
.Li #else
and
.Li #endif
lines are deleted and any lines between them are retained.
When the
.Li #if Ar nothing-but-0
group is followed by
.Li #elif Ar expr ,
it is rewritten to
.Li #if Ar expr
and it is processed as if it were the start of a group.
Similarly,
.Li #elifdef Ar sym
and
.Li #elifndef Ar sym
are rewritten to
.Li #ifdef Ar sym
and
.Li #ifndef Ar sym
respectively.
.\" --------------------------------------------------------------------
.Ss Expression simplification
In
.Li #if
and
.Li #elif
lines in source files,
.Nm
expands macros and evaluates and simplifies
the controlling expression as follows:
.Bl -bullet
.It
Macros and defined values are expanded.
Any
.Sy defined Ns Li \&( Ns Ar sym Ns Li \&)
operators are replaced by 1 if the symbol is defined,
by 0 if it is explicily undefined,
or left alone if the symbol is unknown.
.It
The expression is evaluated.
If there is a syntax error,
the value of the whole expression is unknown.
If a subexpression still contains an unexpanded symbol or
.Sy defined
operator,
the value of just that subexpression is unknown.
.It
The
.Ql \&! ,
.Ql not ,
.Ql && ,
.Ql and ,
.Ql || ,
.Ql Li or ,
and
.Ql ?:
operators are simplified.
When an operand has a known value,
part or all of the subexpression is deleted
(but
.Nm
remembers its value).
Brackets around a subexpression that simplifies to nothing
are also deleted.
.It
A subexpression is only deleted if it contained an expanded symbol or
.Sy defined
operator.
.It
If a deleted subexpression is an argument of another operator
then a
.Li 0
or
.Li 1
is inserted instead.
.El
.Bl -column -offset indent -------------------- --------------------
.It Sy expression                            Ta Sy simplification
.It           Li \&! Ar true                 Ta nothing but 0
.It           Li \&! Ar false                Ta nothing but 1
.It           Li \&! Ar other                Ta unchanged
.It Ar  false Li &&  Ar any                  Ta nothing but 0
.It Ar    any Li &&  Ar false                Ta nothing but 0
.It Ar   true Li &&  Ar true                 Ta nothing but 1
.It Ar   left Li &&  Ar true                 Ta Ar left
.It Ar   true Li &&  Ar right                Ta Ar right
.It Ar   left Li &&  Ar right                Ta unchanged
.It Ar   true Li ||  Ar any                  Ta nothing but 1
.It Ar    any Li ||  Ar true                 Ta nothing but 1
.It Ar  false Li ||  Ar false                Ta nothing but 0
.It Ar   left Li ||  Ar false                Ta Ar left
.It Ar  false Li ||  Ar right                Ta Ar right
.It Ar   left Li ||  Ar right                Ta unchanged
.It Ar   true Li \&? Ar then Li \&: Ar else  Ta Ar then
.It Ar  false Li \&? Ar then Li \&: Ar else  Ta Ar else
.It Ar  other Li \&? Ar then Li \&: Ar else  Ta unchanged
.It Ar truthy Li ?:  Ar any                  Ta Ar truthy
.It Ar  false Li ?:  Ar any                  Ta Ar any
.It Ar  other Li ?:  Ar any                  Ta unchanged
.El
.\"  ___ ___   _   ___ _  _  ___  ___ _____ ___ ___ ___
.\" |   \_ _| /_\ / __| \| |/ _ \/ __|_   _|_ _/ __/ __|
.\" | |) | | / _ \ (_ | .` | (_) \__ \ | |  | | (__\__ \
.\" |___/___/_/ \_\___|_|\_|\___/|___/ |_| |___\___|___/
.\"
.Sh DIAGNOSTICS
.\"  ___ _____ _   _  _ ___   _   ___ ___  ___
.\" / __|_   _/_\ | \| |   \ /_\ | _ \   \/ __|
.\" \__ \ | |/ _ \| .` | |) / _ \|   / |) \__ \
.\" |___/ |_/_/ \_\_|\_|___/_/ \_\_|_\___/|___/
.\"
.Sh STANDARDS CONFORMANCE
It is not possible for
.Nm
to straightforwardly conform to the C and/or C++ standards,
because unlike the standard preprocessor it retains comments and whitespace
and it has a different treatment of unknown preprocessor symbols.
.Pp
This section describes how
.Nm
behaves compared to the standards and other implementations.
Each paragraph is marked as follows:
.Bd -offset indent
.Em (diff)
describes differences between
.Nm
and the standards;
.Pp
.Em (impl)
describes implementation-defined behaviours;
.Pp
.Em (undef)
describes how
.Nm
handles certain undefined behaviour;
.Pp
.Em (vary)
describes how
.Nm
reconciles differences between the various C and C++ standards
and/or popular implementations.
.Ed
.Pp
For instance,
.Pp
.Em (impl)
There is no limit on the size of tokens or depth of nesting
in source files processed by
.Nm
other than available memory.
.\" --------------------------------------------------------------------
.Ss Whitespace and comments
.Em (diff)
The
.Nm
utility passes whitespace and comments through unchanged.
The standard C preprocessor eliminates comments
and backslash-newline sequences,
and may collapse horizontal whitespace.
.Pp
.Em (vary)
C++23 and some C implementations (such as
.Xr gcc 1
and
.Xr clang 1 )
allow
horizontal space inside a backslash-newline sequence,
but
.Nm
does not allow it.
.Pp
.Em (impl)
.Em (vary)
Newlines in source files can be any of
.Ql \en
(line feed),
.Ql \er\en
(CRLF),
or bare
.Ql \er
(carriage return).
This behaviour is required by C++23,
but is implementation-defined in earlier standards and in C.
.Pp
.Em (diff)
The
.Ql \ev
(vertical tab)
and
.Ql \ef
(form feed)
characters are always treated as horizontal whitespace by
.Nm .
In C and C++ they are forbidden in preprocessor lines,
and in C++ they are also forbidden in
.Li //
comments.
.Pp
.Em (vary)
C did not support
.Li //
line comments before C99.
.Pp
.Em (vary)
C requires a newline at the end of a source file;
C++ appends any missing newline;
.Nm
behaves as if the newline were not missing,
but does not append it to the output.
.\" --------------------------------------------------------------------
.Ss Character sets
.Pp
.Em (impl)
Source files are expected to be UTF-8, but
.Nm
allows any ASCII-compatible character set.
.Pp
.Em (vary)
C++23 and
.Nm
allow source files to start with a byte order mark.
.Pp
.Em (vary)
Trigraphs were removed in C++17 and C23;
.Nm
does not support trigraphs.
.Pp
.Em (vary)
Universal character names
(the numeric
.Ql \euXXXX
notation)
appeared in C++98 and C99.
Named universal characters
(the descriptive
.Ql \eN{name}
notation)
appeared in C++23.
.Pp
.Em (vary)
In translation phase 1 of C++11 up to C++20,
characters that are not in the basic source character set
are translated into universal character names.
In translation phase 3 of C++23,
universal character names are decoded into the translation character set.
C and
.Nm
do not do this.
.Pp
.Em (diff)
C and C++ restrict which code points are permitted
in universal character names but
.Nm
does not.
.Pp
.Em (undef)
Unexpected characters, such as control characters or
.Li \(aq
or
.Li \(dq
outside a constant or literal,
are passed through by
.Nm
but cause undefined behaviour in C and C++.
.\" --------------------------------------------------------------------
.Ss Identifiers
.Em (vary)
C before C23 and C++ before C++20
allow identifiers to contain
additional implementation-defined characters.
.Pp
.Em (impl)
Like many compilers,
.Nm
allows
.Ql $
in identifiers.
.Pp
.Em (impl)
UTF-8 characters and other source bytes with the top bit set
are also allowed in identifiers.
.Pp
.Em (diff)
C and C++ allow a subset of universal character names in identifiers,
whereas
.Nm
does not check them.
.Pp
.Em (diff)
Identifiers are matched bytewise;
.Nm
does not canonicalize different spellings of the same character:
the UTF-8 encoding and the various kinds of universal character name
for the same character are treated as different.
.\" --------------------------------------------------------------------
.Ss Integer and floating constants
.Em (vary)
In C++14, C23, GNU C, and
.Nm
you can use binary integer constants like
.Ql 0b0101010 .
.Pp
.Em (vary)
In C++14 and C23
you can separate digit groups in numbers with apostrophes, like
.Ql 1'048'576 .
Digit separators can be read and passed through by
.Nm ,
but it cannot evaluate integers containing them.
.Pp
.Pp
.Em (diff)
Backslash-newline sequences inside integers also prevent
.Nm
from evaluating them.
.Pp
.Em (vary)
The
.Vt long long
type and its
.Ql ll
and
.Ql LL
integer constant suffixes
were introduced in C99 and C++11.
The standard preprocessor and
.Nm
recognise length suffixes but always
evaluate expressions using maximum-width integers,
.Pp
.Em (vary)
The standard preprocessor and
.Nm
do not evaluate floating constants,
which may also contain apostrophes in C++.
.Pp
.Em (vary)
The standard preprocessor and
.Nm
do not evaluate user-defined numeric literals,
which were added in C++11.
.\" --------------------------------------------------------------------
.Ss Character constants
.Pp
.Em (diff)
The following kinds of character constants can be evaluated by
.Nm .
Other character constants are passed through unevaluated.
.Bl -bullet
.It
A single universal character name that does not overflow
.It
A single hex or octal escape that does not overflow
.It
One simple escape like
.Li '\en' ,
with the usual ASCII value,
including the GNU extension
.Li '\ee'
for
.Li '\e033'
ESC
.It
A single ASCII or UTF-8 character,
whose value is the the corresponding Unicode code point
.It
A single byte with the top bit set
(when the source is non-UTF-8 extended ASCII)
.El
.Pp
.Em (impl)
Hex and octal escape sequences can be up to 8 bits for bare character constants,
7 bits for
.Li u8'X'
constants,
16 bits for
.Li u'X'
constants,
or 32 bits for
.Li U'X'
or
.Li L'X'
constants.
.Pp
.Em (impl)
Bare character constants without an encoding prefix are sign-extended if
their value is between 128 and 255, unless they are UTF-8.
.Pp
.Em (impl)
Wide character constants containing a non-UTF8 extended ASCII byte with an
.Ql L
encoding prefix are also sign-extended.
.Pp
.Em (impl)
The full range of UTF-8 values is allowed in most character constants.
The standards limit
.Li u'X'
to 16 bits,
and
.Li u8'X'
to 7 bits.
.Pp
.Em (vary)
The
.Ql U
and
.Ql u
encoding prefixes were introduced in C11 and C++11.
They are recognised by
.Nm .
.Pp
.Em (vary)
The
.Ql u8
character prefix was introduced in C++14 and C2x.
It is not supported by
.Xr gcc 1
nor
.Xr clang 1
but is supported by
.Nm .
.Pp
.Em (vary)
User-defined character constants were introduced in C++11.
They are recognized and passed through by
.Nm
but they cannot be evaluated in
.Li #if
preprocessor control expressions.
.\" --------------------------------------------------------------------
.Ss String literals
.Em (vary)
The
.Ql U ,
.Ql u ,
and
.Ql u8
string encoding prefixes were introduced in C11 and C++11.
They are all recognised by
.Nm .
.Pp
.Em (vary)
C++11 added multiline raw string literals like
.Li R\(dqdelim(contents)delim\(dq
and user-defined string literals like
.Li \(dqstring\(dqtag
are also recognised by
.Nm .
.\" --------------------------------------------------------------------
.Ss Alternative spellings
.Em (vary)
In C99, you can use wordy versions of certain operators after
.Li #include
.In iso646.h .
In C++ and
.Nm
the wordy operators are built in.
.Pp
.Em (vary)
In C99, you can use
.Li true
and
.Li false
as constants after
.Li #include
.In stdbool.h .
In C++ and
.Nm ,
.Li true
and
.Li false
are built in.
.Pp
.Em (vary)
Digraphs appeared in C++98 and C99.
The
.Ql %\&:
and
.Ql %\&:\&%\&:
aliases for
.Ql #
and
.Ql ##
are supported by
.Nm .
(The other digraphs are not relevant to the C preprocessor.)
.\" --------------------------------------------------------------------
.Ss Preprocessor directives
.Em (vary)
C2x added the
.Li #elifdef
and
.Li #elifndef
directives,
which are supported by
.Nm .
.Pp
.Em (diff)
The
.Li #defval
directive is specific to
.Nm Fl f Pa header
files.
For compatibility with standard compilers,
it can be written
.Li "#pragma unifdef defval" .
.\" --------------------------------------------------------------------
.Ss Expression evaluation
.Em (impl)
The C preprocessor is required to evaluate
.Li #if
controlling expressions using
.Li intmax_t
and
.Li uintmax_t .
The details of these types are determined by the compiler that
.Nm
was built with.
.Pp
.Em (diff)
When there is an error during evaluation,
.Nm
produces an unknown result,
so that the problem subexpression is not simplified.
.Pp
.Em (diff)
Unknown symbols evaluate to 0 in the standard preprocessor,
but produce an unknown result in
.Nm .
.Pp
.Em (diff)
A wordy operator that appears in an operator position, like
.Ql "5 xor 10" ,
acts as an operator, for compatibility with C++.
A wordy operator that appears in an operand position after macro expansion,
like
.Ql "-xor" ,
is treated as an unknown symbol for compatibility with C.
However
.Ql not
is always parsed as an operator.
.Pp
.Em (vary)
GNU C and
.Nm
support a binary variant of the
.Ql ?:
operator, where
.Ar then Li ?: Ar else
is like
.Ar then Li \&? Ar then Li \&: Ar else
except the
.Ar then
operand is written and evaluated once.
.Pp
.Em (undef)
Signed integer overflow in multiplication, addition, subtraction, or negation
produces an unknown result.
.Pp
.Em (undef)
When evaluating the
.Li /
or
.Li %
operators,
division by zero produces an unknown result,
as does division of
.Li INTMAX_MIN
by
.Li -1
(which causes an overflow because
.Li -INTMAX_MIN Li > Li INTMAX_MAX Ns ).
.Pp
.Em (vary)
Shift operators in C and C++ consist almost entirely of undefined behaviour.
In C++20 shifts are more tightly specified, and
.Nm
implements the new requirements, as follows.
.Pp
.Em (undef)
Negative shifts or shifts of more than the word size
produce an unknown result.
.Pp
.Em (impl)
A right shift of a negative value is an arithmetic shift,
so
.Ar left Li >> Ar right
is always
.Ar left Li / 2^ Ns Ar right .
.Pp
.Em (undef)
A left shift is the same for signed and unsigned values.
Before C++20, a left shift of a negative value is undefined,
as is a left shift of a positive value that overflows into the sign bit.
.\" --------------------------------------------------------------------
.Ss Macro definitions
.Em (diff)
There are no predefined macros in
.Nm .
.Pp
.Em (diff)
The
.Li __VA_ARGS__
and
.Li __VA_OPT__
identifiers may occur only in the body of a variadic macro,
but
.Nm
does not check this.
.Pp
.Em (undef)
Other reserved symbols
(starting with underscore uppercase or double underscore)
and standard predefined macros are not restricted by
.Pp
.Em (vary)
Variadic macros were introduced in C99 and C++11.
The
.Li __VA_OPT__
pseudo-macro was introduced in C++20.
In GNU C you can use the
.Li ##
operator before the variable arguments
instead of
.Li __VA_OPT__ .
You can use either in
.Nm .
.Pp
.Em (vary)
The
.Li __VA_ARGS__ ,
.Li __VA_OPT__ ,
and
.Sy defined
keywords may not be defined or undefined.
.Nm .
.\" --------------------------------------------------------------------
.Ss Macro expansion
.Em (undef)
The standards say that
.Sy defined Ns Li \&( Ns Ar sym Ns Li \&)
operators are evaluated before macro expansion,
and macros that expand to
.Sy defined
operators cause undefined behaviour.
In practice,
implementations (such as
.Xr gcc 1
and
.Xr clang 1
and
.Nm )
evaluate
.Sy defined
operators like a special kind of macro,
so it works as expected when used in a macro body.
.Pp
.Em (vary)
In C++, a user-defined literal suffix is part of the token, but
.Nm
and the
.Xr gcc 1
and
.Xr clang 1
preprocessors treat it as a separate identifier token
that may be expanded as macro.
If you define a macro and write its name immediately after a string
or character literal, a standard preprocessor should not expand the macro
(unless its spelling is the same as a keyword).
.\"  __  __ ___ ___  ___ ___ _    _      _   _  _ ___   _
.\" |  \/  |_ _/ __|/ __| __| |  | |    /_\ | \| | __| /_\
.\" | |\/| || |\__ \ (__| _|| |__| |__ / _ \| .` | _| / _ \
.\" |_|  |_|___|___/\___|___|____|____/_/ \_\_|\_|___/_/ \_\
.\"
.Sh SEE ALSO
.Xr cpp 1 ,
.Xr diff 1 .
.Pp
.Lk https://dotat.at/prog/unifdef3/ "The unifdef home page"
.Pp
ISO/IEC 9899 (C),
ISO/IEC 14882 (C++).
.Pp
The C standards committee,
.Lk http://www.open-std.org/jtc1/sc22/wg14/ "ISO/IEC JTC1/SC22/WG14"
.Pp
The C++ standards committee,
.Lk http://www.open-std.org/jtc1/sc22/wg21/ "ISO/IEC JTC1/SC22/WG21"
.\" --------------------------------------------------------------------
.Sh HISTORY
The
.Nm
command appeared in
.Bx 4.1c .
ANSI\~C
support was added in
.Nm
version 2 which appeared in
.Fx 4.7 .
Support for C++
and macro expansion
was added in
.Nm
version 3.
.\" --------------------------------------------------------------------
.Sh AUTHORS
.An Dave Yost Aq Dave@Yost.com
wrote the original K&R\~C implementation.
.An Tony Finch Aq dot@dotat.at
rewrote
.Nm
to support ANSI\~C,
and again to support C++
and macro expansion.
.Pp
Please refer to
.Lk https://dotat.at/prog/unifdef3/ the unifdef home page
to report bugs or request features.
.\" --------------------------------------------------------------------
