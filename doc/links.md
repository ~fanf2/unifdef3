Reference documents
-------------------

  - <https://www.spinellis.gr/blog/20060626/>

    Dave Prosser's ANSI C preprocessor algorithm. Useful guide, but
    lacks C99 things like variadic macros and `##` placemarkers.

  - <https://gcc.gnu.org/onlinedocs/cppinternals/>

    GNU C preprocesor internals. A good discussion of some knotty
    implementation details. GNU cpp and most other modern C
    preprocessors are token-based, but unifdef is textual,
    so GNU cpp is more useful for comparison than as a guide.

  - <https://www.corsix.org/content/va-opt-minutiae>

    Peter Cawley's notes on `__VA_OPT__` in C23 and C++23.

  - <https://jadlevesque.github.io/PPMP-Iceberg/>

    The C preprocessor iceberg has many tricky examples in its
    links, many of which go to the iceberg's own explanations page.

  - <https://www.oilshell.org/blog/2017/03/31.html>

    A collection of articles on Pratt parsing. The unifdef #if
    expression simplifier uses a table-driven Pratt-style top-down
    operator precedence parser / evaluator.


Other C preprocessors
---------------------

Not including preprocessors built in to compiler front-ends...

  - <https://github.com/paulross/cpip>

    CPIP, a C preprocessor written in Python with lots of tools for
    visualizing and inspecting what it does.

  - <http://mcpp.sourceforge.net/>

    The mcpp portable C preprocessor and validation suite. The
    documentation discusses some trouble spots and weird uses
    of the preprocessor that its author found in the wild.

  - <https://github.com/boostorg/wave>

    Boost Wave C preprocessor, a library written in boosted C++

  - <https://github.com/notfoundry/ppstep>

    PPstep, a preprocessor debugger with step-by-step macro expansion.
