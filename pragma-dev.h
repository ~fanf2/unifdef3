#line 2 "pragma-dev.h"

#define like_printf(n,m) __attribute__((format(printf,n,m)))
#define noreturn __attribute__((__noreturn__))
#define unused   __attribute__((__unused__))

#pragma GCC diagnostic error "-Wall"
#pragma GCC diagnostic error "-Wextra"
#pragma GCC diagnostic error "-Wpedantic"
#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#pragma clang diagnostic error "-Weverything"
// not too worried about a little wasted space
#pragma clang diagnostic ignored "-Wpadded"
// -Wcovered-switch-default helpfully warns if there is a redundant
// default: but -Wswitch-default unhelpfully warns if there is a
// missing default: even if it would be redundant; disable the less
// helpful warning so we can compile with -Werror
#pragma clang diagnostic ignored "-Wswitch-default"
// missing case():s are ok when we have default:
#pragma clang diagnostic ignored "-Wswitch-enum"
// gcc treats unused as maybe_unused but clang is more strict
#pragma clang diagnostic ignored "-Wused-but-marked-unused"
// why has clang started warning about pre-C99 20 years later?
#pragma clang diagnostic ignored "-Wdeclaration-after-statement"
// this warning has many unavoidable false positives
#pragma clang diagnostic ignored "-Wunsafe-buffer-usage"
